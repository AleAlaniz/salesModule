<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Campaign_model');
		$this->load->model('Product_model');
	}

	public function index()
	{	
		if($this->Identity_model->Validate('campaigns/view'))
		{
			$this->load->view('_shared/header');
			$this->load->view('campaigns/index', array('campaigns' => $this->Campaign_model->GetCampaigns()));
			$this->load->view('_shared/footer');
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function view()
	{
		$campaign_id = $this->uri->segment(3);
		if($this->Identity_model->Validate('campaigns/view') && isset($campaign_id))
		{
			$this->_showCampaignInfo($campaign_id, __FUNCTION__);
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function create()
	{
		if($this->Identity_model->Validate('campaigns/admin'))
		{
			$rules = array(
				array(
					'field' => 'campaign_name',
					'label' => 'lang:campaign_name',
					'rules' => 'required|min_length[1]|max_length[50]|is_unique_active[campaigns.name]'
				),
				array(
					'field' => 'campaign_description',
					'label' => 'lang:campaign_description',
					'rules' => 'min_length[1]|max_length[100]'
				),
			);

			if (isset($_POST['campaignProducts'])){

				array_push($rules, array(
					'field' => 'campaignProducts[]',
					'label' => 'lang:campaignProducts',
					'rules' => 'callback_products_check[campaignProducts]'
				));
				
			}

			if(!isset($_POST['campaign_extra_data_h']) || $_POST['campaign_extra_data_h'] == 0){
				$_POST['campaign_extra_data_required_h'] = 0;
			}

			$this->form_validation->set_rules($rules);

			if(! $this->form_validation->run())
			{

				$this->_showCreateView();
				return;
			}

			$campaign_id = $this->Campaign_model->Create();
			
			if(! isset($campaign_id))
			{
				$this->_showCreateView($this->lang->line('general_database_error'));
			}
			else
			{
				$this->session->set_flashdata('saveResult', $this->lang->line('campaign_create_success'));
				header('Location:/'.FOLDERADD.'/campaigns/view/'.$campaign_id);
			}			
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function edit()
	{
		$url_id = $this->uri->segment(3);
		
		if($this->Identity_model->Validate('campaigns/admin') && isset($url_id))
		{
			$rules = array(
				array(
					'field' => 'campaign_id',
					'label' => 'lang:campaign_id',
					'rules' => 'required|numeric|in_list['.$url_id.']|exist_campaign'
				),
				array(
					'field' => 'campaign_name',
					'label' => 'lang:campaign_name',
					'rules' => 'required|min_length[1]|max_length[50]|edit_unique[campaigns.name.campaignId.'.$url_id.'.TRUE]'
				),
			);

			if (isset($_POST['campaignProducts'])){

				array_push($rules, array(
					'field' => 'campaignProducts[]',
					'label' => 'lang:campaignProducts',
					'rules' => 'callback_products_check[campaignProducts]'
				));
				
			}
			
			if(!isset($_POST['campaign_extra_data_h']) || $_POST['campaign_extra_data_h'] == 0){
				$_POST['campaign_extra_data_required_h'] = 0;
			}
			
			$this->form_validation->set_rules($rules);

			if(! $this->form_validation->run())
			{
				$this->_showCampaignInfo($url_id, __FUNCTION__);
				return;
			}

			$campaign_id = $this->Campaign_model->Edit();

			if(! isset($campaign_id))
			{
				$this->_showCampaignInfo($url_id, __FUNCTION__, $this->lang->line('general_database_error'));
			}
			else
			{
				$this->session->set_flashdata('saveResult', $this->lang->line('campaign_edit_success'));
				header('Location:/'.FOLDERADD.'/campaigns/view/'.$campaign_id);
			}
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function delete()
	{
		$url_id = $this->uri->segment(3);

		if($this->Identity_model->Validate('campaigns/admin') && isset($url_id))
		{
			$rules = array(
				array(
					'field' => 'campaign_id',
					'label' => 'lang:campaign_id',
					'rules' => 'required|numeric|in_list['.$url_id.']|exist_campaign'
				));

			$this->form_validation->set_rules($rules);

			if(! $this->form_validation->run())
			{
				$this->_showCampaignInfo($url_id, __FUNCTION__);
				return;
			}

			$campaign_id = $this->input->post('campaign_id', TRUE);

			if(! $this->Campaign_model->Delete())
			{
				$this->_showCampaignInfo($campaign_id, __FUNCTION__, $this->lang->line('general_database_error'));
			}
			else
			{
				$this->session->set_flashdata('deleteResult', $this->lang->line('campaign_delete_success'));
				header('Location:/'.FOLDERADD.'/campaigns');
			}
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function getProducts()
	{
		$data = new Stdclass();
		$data = $this->Product_model->getCampaignProducts($_POST['campaignId']);
		echo json_encode($data);
	}

	public function search_campaigns()
	{
		if ($this->Identity_model->Validate('campaigns/admin') && $this->input->post('search')){

			if($this->input->post('search') == NULL){

				echo json_encode(array());
				return;
			}

			$searchString = strtolower(substr(trim($this->input->post('search', TRUE)), 0, 50));
			echo json_encode ($this->Campaign_model->SearchCampaigns($searchString));

		}
		else{
			show_404();
		}
	}

	//Llamada a vistas

	private function _showCreateView($error = NULL)
	{
		$this->load->view('_shared/header');
		$this->load->view('campaigns/create', array('error' => $error));
		$this->load->view('_shared/footer');
	}

	private function _showCampaignInfo($campaign_id, $type, $error = NULL)
	{
		if(! (isset($campaign_id) && is_numeric($campaign_id) && $this->form_validation->exist_campaign($campaign_id)))
		{
			header('Location:/'.FOLDERADD);
			return;
		}

		$this->load->view('_shared/header');
		$this->load->view('campaigns/'.$type, array('campaign_data' => $this->Campaign_model->GetCampaign($campaign_id), 'error' => $error));
		$this->load->view('_shared/footer');
	}

	//Validaciones personalizadas
	public function exist_user($user)
	{
		$response = TRUE;
		if(strlen($user) > 0)
		{
			$this->db->where('userId', $user);
			$this->db->where('active', 1);
			$this->db->from('users');

			if($this->db->count_all_results() == 0)
			{
				$this->form_validation->set_message('exist_user', $this->lang->line('campaign_error_user_not_exist'));
				$response = FALSE;
			}
		}

		return $response;
	}

	public function user_unique_in_list($user, $field_name)
	{
		$users_list = $this->input->post($field_name);
		$usersCount = count($users_list);
		$userEncounters = 0;

		for($i = 0; $i < $usersCount; $i++)
		{
			if($user == $users_list[$i])
			{
				$userEncounters++;
				if($userEncounters >= 2)
					break;
			}
		}

		if($userEncounters > 1)
		{
			$this->form_validation->set_message('user_unique_in_list', $this->lang->line('campaign_error_user_exist_in_list'));
			return FALSE;
		}

		return TRUE;
	}

	public function products_check()
	{
		$product_list = $_POST['campaignProducts'];

		$count = 0;
		foreach ($product_list as $key => $product){
			
			if ((strlen($product['name']) == 0) || (strlen($product['detail'] == 0))){

				$this->form_validation->set_message('products_check', $this->lang->line('campaign_error_product_required'));
				return FALSE;
			}
		}

		foreach ($product_list as $key => $product){

			$name = $product['name'];

			foreach ($product_list as $key => $productCompare){

				if ($productCompare['name'] == $name){

					$count++;
					if($count >= 2)
						break;
				}
			}

			if ($count > 1){

				$this->form_validation->set_message('products_check', $this->lang->line('campaign_error_product_name_unique_in_list'));
				return FALSE;
			}	
		}

		return TRUE;
	}
}
