<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Campaign_model');
	}

	public function index()
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			header('Location:/'.FOLDERADD.'/sales/create');
		}
		else{
			$this->load->view('users/login');
		}
	}


	public function autenticate(){
		$_SESSION['auth_status'] = $this->Identity_model->Autenticate();
		$this->session->mark_as_flash('auth_status');

		if (strcmp('success', $_SESSION['auth_status']) === 0){
			//data = $this->Campaign_model->GetCampaignsWithProducts();
            //this->load->view('_shared/header', array('my_campaigns' => $this->Campaign_model->GetMyCampaigns()));
            //this->load->view('sales/create', array('data' => $data));
            //this->load->view('_shared/footer');
            header('Location:/'.FOLDERADD.'/sales/create');
		}

		$this->load->view('users/login');
	}

	public function logout()
	{
		$this->Identity_model->Logout();
	}
}
