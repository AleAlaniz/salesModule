<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reports_model');
		$this->load->model('Campaign_model');

		//require_once 'Spreadsheet/Excel/Writer.php';
	}

	public function index()
	{	
		if($this->Identity_model->Validate('reports/viewall') || $this->Identity_model->Validate('reports/viewonly'))
		{
			$dataForView = array(
				'campaigns' 	=> $this->Campaign_model->GetCampaignsForm()
			);
			if(isset($_POST['export_sales_report']))
			{
				
				$data = $this->Reports_model->GetSalesExcel();
				
				$workbook = new Spreadsheet_Excel_Writer(); 
				$workbook->send('sales.xls');

				$format_bold = $workbook->addFormat();
				$format_bold->setBold();

				$worksheet = $workbook->addWorksheet('Ventas');

				$worksheet->write(0, 0, $this->lang->line('report_contract_number'), $format_bold);
				$worksheet->write(0, 1, $this->lang->line('report_username'), $format_bold);
				$worksheet->write(0, 2, $this->lang->line('report_agent'), $format_bold);
				$worksheet->write(0, 3, $this->lang->line('report_team_leader'), $format_bold);
				$worksheet->write(0, 4, utf8_decode($this->lang->line('report_campaign')), $format_bold);
				$worksheet->write(0, 5, $this->lang->line('report_product'), $format_bold);
				$worksheet->write(0, 6, $this->lang->line('report_comment'), $format_bold);
				$worksheet->write(0, 7, $this->lang->line('report_sale_date'), $format_bold);
				$worksheet->write(0, 8, $this->lang->line('report_dni'), $format_bold);
				$worksheet->write(0, 9, utf8_decode($this->lang->line('report_phone')), $format_bold);
				$worksheet->write(0, 10, $this->lang->line('sales_census'), $format_bold);
				$worksheet->write(0, 11, $this->lang->line('sales_commercial_offer'), $format_bold);

				$dataCount = count($data); 
				for($i = 0; $i < $dataCount ; $i++)
				{
					$worksheet->writeString($i+1, 0, $data[$i]->contractNumber);
					$worksheet->writeString($i+1, 1, $data[$i]->userName);
					$worksheet->writeString($i+1, 2, utf8_decode($data[$i]->completeName));
					$worksheet->writeString($i+1, 3, utf8_decode($data[$i]->teamLeader));
					$worksheet->writeString($i+1, 4, utf8_decode($data[$i]->campaignName));
					$worksheet->writeString($i+1, 5, utf8_decode($data[$i]->productName));
					$worksheet->writeString($i+1, 6, utf8_decode($data[$i]->comment));
					$worksheet->writeString($i+1, 7, $data[$i]->saleDate);
					$worksheet->writeString($i+1, 8, $data[$i]->dni);
					$worksheet->writeString($i+1, 9, $data[$i]->phone);
					$worksheet->writeString($i+1, 10, $data[$i]->census);
					$worksheet->writeString($i+1, 11, $data[$i]->commercial_offer);
				}

				$workbook->close();
				header("Content-type: application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=excel.xls");
				header("Pragma: no-cache");
				header("Expires: 0");
			}
			else
			{
				$this->load->view('_shared/header', array("max_width" => TRUE));
				$this->load->view('reports/index', $dataForView);
				$this->load->view('_shared/footer');
			}
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function getsales()
	{
		
		$customEndDate = null;

		$startDate = date_create_from_format('d/m/Y', $this->input->post('start_date'));
		$endDate = date_create_from_format('d/m/Y', $this->input->post('end_date'));

		$customEndDate = $endDate->format('d/m/Y');
		if(date_diff($startDate, $endDate)->days > 30)
		{
			$startDate->add(new DateInterval('P30D'));
			$customEndDate = $startDate->format('d/m/Y');				
		}
		$campaigns = $this->input->post('campaigns');
		
		$this->Reports_model->GetSales($customEndDate,$campaigns);
	}

	public function date_is_greater_than($value, $other)
	{
		$start_date	 = date_create_from_format('d/m/Y', $this->input->post($other));
		$end_date	 = date_create_from_format('d/m/Y', $value);

		if($start_date == FALSE || $end_date == FALSE || $start_date->getTimestamp() > $end_date->getTimestamp() )
		{
			$this->form_validation->set_message('date_is_greater_than', $this->lang->line('report_date_greater_error'));
			return FALSE;
		}

		return TRUE;
	}
}
