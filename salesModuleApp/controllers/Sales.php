<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Sales_model','Sales');
        $this->load->model('Campaign_model');
    }

    public function create()
    {
        if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
        {
            if($this->session->selectedCampaign)
            {
                $sql = "SELECT uc.userId
                FROM campaigns c JOIN usersInCampaigns uc
                ON (c.campaignId = uc.campaignId)
                WHERE uc.userId = ? AND c.campaignId = ? AND c.active = 1";

                $result = $this->db->query($sql, array($this->session->UserId, $this->session->selectedCampaign))->row();
                
                if(!isset($result))
                {
                    $this->Identity_model->SetSelectedCampaign();
                }
            }
            else
            {
                $this->Identity_model->SetSelectedCampaign();
            }

            $this->form_validation->set_rules('change_campaign_id', '', 'numeric|exist_campaign');
            if($this->form_validation->run())
            {

                $sql = "SELECT userId FROM usersInCampaigns WHERE userId = ?";
                $res = $this->db->query($sql, $this->session->UserId);

                if(isset($res))
                {
                    $this->session->selectedCampaign = $this->input->post('change_campaign_id');
                }
            }

            $data = $this->Campaign_model->GetCampaignsWithProducts();
            $this->load->view('_shared/header', array('my_campaigns' => $this->Campaign_model->GetMyCampaigns()));
            $this->load->view('sales/create', array('data' => $data));
            $this->load->view('_shared/footer');
        }
        else
        {
            header('Location:/'.FOLDERADD.'/');
        }
    }

    public function createDb()
    {
        $data = $_POST;
        
        if(count($data) > 0){
            if($this->Identity_model->Validate('sales/admin'))
            {   
                $this->form_validation->set_data($data);
                
                $this->form_validation->set_rules('contractNumber'  , 'lang:sales_contractNumber'   , 'required|numeric');
                $this->form_validation->set_rules('product'         , 'lang:main_product'           , 'required|numeric|exist_product');
                $this->form_validation->set_rules('comment'         , 'lang:sales_comment'          , 'htmlspecialchars|max_length[500]');
                $this->form_validation->set_rules('phone'           , 'lang:sales_phone'            , 'required|numeric|min_length[6]|max_length[15]');
                $this->form_validation->set_rules('dni'             , 'lang:sales_dni'              , 'required|numeric|min_length[7]|max_length[8]');

                $sql = "SELECT c.extra_data, c.extra_data_required
                FROM campaigns c
                WHERE active = 1 AND campaignId = ?";
                $campaign_extra_data = $this->db->query($sql, $this->session->selectedCampaign)->row();

                if($campaign_extra_data->extra_data && $campaign_extra_data->extra_data_required){

                    $this->form_validation->set_rules('census'            , 'lang:sales_census'            , 'required|numeric|min_length[8]|max_length[8]');
                    $this->form_validation->set_rules('commercial_offer'  , 'lang:sales_commercial_offer'  , 'required');
                }
                
                if ($this->form_validation->run() == TRUE) {
                    //validar el id de campaña
                    $sql = "SELECT * FROM campaigns WHERE campaignId = ?";
                    $res = $this->db->query($sql,$this->session->selectedCampaign)->row();
                    if(!isset($res))
                    {
                        show_404();
                    }
                    
                    //validar el id de producto
                    $sql = "SELECT * FROM products WHERE productId = ?";
                    $res = $this->db->query($sql,$this->input->post('product'))->row();
                    if(!isset($res))
                    {
                        show_404();
                    }
                    
                    //Validar si el usuario aun sigue en la campaña
                    $sql = "SELECT userId FROM usersInCampaigns WHERE userId = ? AND campaignId = ?";
                    $res = $this->db->query($sql, array($this->session->UserId, $this->session->selectedCampaign))->row();
                    
                    if(!isset($res))
                    {
                        echo 'not_in_campaign';
                        return;
                    }

                    $saleInsert = array(
                        'campaignId'        => $this->session->selectedCampaign, 
                        'userId'            => $this->session->UserId,
                        'productId'         => $this->input->post('product'),
                        'saleDate'          => date('Y-m-d H:i:s'),
                        'contractNumber'    => $this->input->post('contractNumber'),
                        'comment'           => $this->input->post('comment'),
                        'phone'             => $this->input->post('phone'),
                        'dni'               => $this->input->post('dni'),
                        'census'            => $this->input->post('census'),
                        'commercial_offer'  => $this->input->post('commercial_offer')
                    );

                    $res = $this->Sales->Create($saleInsert);
                    if($res == 'success'){
                        echo $res;
                    }
                } 
                else {
                    print_r(validation_errors());
                }
            }
            else{
                show_404();
            }

        }
        else {
            show_404();
        }    
    }

    public function delete()
    {
        if($this->Identity_model->Validate('reports/viewall') && !$this->Identity_model->Validate('reports/viewonly')){ 
            $res = "";
            if(isset($_POST['saleId']))
            {
                if($this->exists($_POST['saleId'])){

                    $res = $this->Sales->Delete($_POST['saleId']);
                    if($res == "success"){
                        $this->session->set_flashdata('saleMessage', 'delete');
                        echo $res; return;
                    }
                    echo $res; return;
                }
                else{
                    show_404();
                }
            }
            $res =  "error no saleId";
            echo $res; return;
        }
        else{
            header('Location:/'.FOLDERADD.'/');
        }
    }

    public function editSale($value='')
    {
        if($this->Identity_model->Validate('reports/viewall') && !$this->Identity_model->Validate('reports/viewonly')){ 
            $res = new StdClass();
            $params = array();
            if(isset($_POST['form'])){

                parse_str($_POST['form'], $params);

                $this->form_validation->set_rules('contractNumber'  , 'lang:sales_contractNumber'   , 'required|numeric');
                $this->form_validation->set_rules('product'         , 'lang:main_product'           , 'required|numeric|exist_product');
                $this->form_validation->set_rules('comment'         , 'lang:sales_comment'          , 'htmlspecialchars|max_length[500]');
                $this->form_validation->set_rules('phone'           , 'lang:sales_phone'            , 'required|numeric|min_length[6]|max_length[15]');
                $this->form_validation->set_rules('dni'             , 'lang:sales_dni'              , 'required|numeric|min_length[7]|max_length[8]');
                $this->form_validation->set_rules('date'            , 'lang:report_sale_date'       , 'required|is_valid_date');
                $this->form_validation->set_rules('hour'            , 'lang:report_sale_hour'       , 'required');
                $this->form_validation->set_rules('saleId'          , 'lang:sale'                   , 'required|callback_sale_exists');

                $sql = "SELECT c.extra_data, c.extra_data_required
                FROM campaigns c
                WHERE active = 1 AND campaignId = ?";

                $campaign_extra_data = $this->db->query($sql, $params['campaignId'])->row();

                if($campaign_extra_data->extra_data && $campaign_extra_data->extra_data_required){

                    $this->form_validation->set_rules('census'            , 'lang:sales_census'            , 'required|numeric|min_length[8]|max_length[8]');
                    $this->form_validation->set_rules('commercial_offer'  , 'lang:sales_commercial_offer'  , 'required');
                }

                $this->form_validation->set_data($params);
    
                if($this->form_validation->run() == false){
    
                    $res->status = 'form-error';
                    $res->message = validation_errors();
                    
                }
                else{
    
                    $sale_edit_res = $this->Sales->edit($params);
    
                    if($sale_edit_res == 1){
    
                        $res->status = 'success';
                        $res->message = 'La venta fue editada exitosamente';
                        $this->session->set_flashdata('saleMessage', 'edit');
                    }
                    else{
                        $res->status = 'failure';
                        $res->message = 'La venta no fue editada debido a un error inesperado';
                    }
                }
    
                echo json_encode($res);
            }
            else{
                header('Location:/'.FOLDERADD.'/');
            }
        }
        else{
            header('Location:/'.FOLDERADD.'/');
        }
    }

    public function exists($saleId)
    {
        if($this->Identity_model->Validate('reports/viewall') && !$this->Identity_model->Validate('reports/viewonly')){ 
            $res = $this->Sales->GetSaleById($saleId);
            if(isset($res))
            {
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
        else{
            header('Location:/'.FOLDERADD.'/');
        }
    }

    public function sale_exists($saleId)
    {

        $res = $this->Sales->GetSaleById($saleId);

        if(isset($res))
            return TRUE;
        
        $this->CI->form_validation->set_message('sale_exists',$this->CI->lang->line('sale_invalid'));
        return FALSE;         
    }

}

/* End of file Sales.php */
