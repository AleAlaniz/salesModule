<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Teamleaders extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Teamleaders_model','Teamleaders');
        }

        public function index()
        {
            if($this->Identity_model->Validate('teamleaders/view')){
                $teamleaders = $this->Teamleaders->getTLs();
                $this->load->view('_shared/header');
                $this->load->view('teamleaders/index', array('teamleaders' => $teamleaders));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:/'.FOLDERADD.'/');
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('teamleaders/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[teamLeaders.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('teamleaders/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $tlInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Teamleaders->Create($tlInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('teamleaderMessage', 'create');
                        header('Location:/'.FOLDERADD.'/teamleaders'); 
                    }
                }
                
            }
            else{
                header('Location:/'.FOLDERADD.'/teamleaders'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('teamleaders/admin')){

                    $teamleaderId = $this->uri->segment(3);
                    $sql = "SELECT * FROM teamLeaders WHERE teamLeaderId = ? AND active = 1";
                    
                    $res = new StdClass();
                    $res->teamLeader = $this->db->query($sql,$teamleaderId)->row();

                    if(isset($res->teamLeader))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[teamLeaders.name.teamLeaderId.'.$teamleaderId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $teamleaderId;

                            $this->load->view('_shared/header');
                            $this->load->view('teamleaders/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'teamLeaderId' => $teamleaderId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Teamleaders->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('teamleaderMessage', 'edit');
                                header('Location:/'.FOLDERADD.'/teamleaders'); 
                            }
                        }
                    }
                    else{
                        header('Location:/'.FOLDERADD.'/');     
                    }
                }
                else{
                    header('Location:/'.FOLDERADD.'/teamleaders');     
                }
            }
            else {
                header('Location:/'.FOLDERADD.'/teamleaders'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('teamleaders/admin')){
                    $teamleaderId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->teamLeader = $this->Teamleaders->getTeamLeader($teamleaderId);
                    
                    if(isset($res->teamLeader))
                    {
                        if($this->input->post('teamleaderId') && $this->input->post('teamleaderId') == $res->teamLeader->teamLeaderId){
                            $teamleaderDelete = array('teamLeaderId' => $res->teamLeader->teamLeaderId);
                            $res = $this->Teamleaders->Delete($teamleaderDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('teamleaderMessage', 'delete');
                                header('Location:/'.FOLDERADD.'/teamleaders');
                            }
                        }
                        else {
                            $res->unique = $teamleaderId;
    
                            $this->load->view('_shared/header');
                            $this->load->view('teamleaders/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:/'.FOLDERADD.'/');
                    }
                }
                else {
                    header('Location:/'.FOLDERADD.'/');
                }
            }   
            else {
                header('Location:/'.FOLDERADD.'/');
            }
        }
    }

    /* End of file Teamleaders.php */
?>