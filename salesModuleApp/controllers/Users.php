<?php   
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','Users');
    }

    public function index()
    {   
        if($this->Identity_model->Validate('users/view')){
            $users = $this->Users->GetUsers();
            $this->load->view('_shared/header');
            $this->load->view('users/index', array('users' => $users));
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function create()
    {
        if($this->Identity_model->Validate('users/admin')){

            $this->form_validation->set_rules('userName'    ,'lang:login_username'         ,'is_unique_active[users.userName]|required|max_length[50]');
            $this->form_validation->set_rules('password'    ,'lang:login_password'         ,'required|min_length[4]|max_length[128]');
            $this->form_validation->set_rules('confirmPassword', 'lang:admin_users_confirm_password', 'required|matches[password]',
                array(
                    'matches'     => $this->lang->line('admin_users_passwordmatch')
                ));
                $this->form_validation->set_rules('user_campaigns[]', 'lang:main_campaigns'      , 'numeric|callback_campaign_exists|callback_unique_campaign[user_campaigns[]]');
            $this->form_validation->set_rules('name'        ,'lang:general_name'           ,'required|max_length[50]');
            $this->form_validation->set_rules('lastName'    ,'lang:admin_users_lastName'   ,'required|max_length[50]');
            $this->form_validation->set_rules('turn'        ,'lang:admin_users_turn'       ,'required|max_length[50]|callback_check_turn');
            $this->form_validation->set_rules('cuil'        ,'lang:admin_users_cuil'       ,'required|min_length[11]|max_length[11]|numeric');
            $this->form_validation->set_rules('role'        ,'lang:admin_users_role'        ,'exist_role');

            if ($this->form_validation->run() == FALSE) {

                $data = $this->Users->getFormData();

                if($this->input->post('user_campaigns')){

                    $campaignIds = $this->input->post('user_campaigns');

                    $this->db->select('campaignId, name');
                    $this->db->where_in('campaignId',$campaignIds);

                    $data->user_campaigns = $this->db->get('campaigns')->result();
                }
                
                $this->load->view('_shared/header');
                $this->load->view('users/create',$data);
                $this->load->view('_shared/footer');

            } 
            else 
            {
                //para validar si el id de teamleader enviado está en la base de datos
                if($this->input->post('teamLeader') && $this->input->post('teamLeader') != "EMPTY")
                {
                    $sql = "SELECT * FROM teamLeaders WHERE teamLeaderId = ?";
                    $result = $this->db->query($sql,$this->input->post('teamLeader'))->row();

                    if(!isset($result))
                    {
                        show_404();
                    }
                }

                //para validar si el id de role enviado está en la base de datos
                if($this->input->post('role') && $this->input->post('role') != "EMPTY"){

                    $roleId = $this->input->post('role') ? $this->input->post('role') : $this->input->post('roleH');

                    $sql = "SELECT * FROM roles WHERE roleId = ?";
                    $result = $this->db->query($sql,$roleId)->row();

                    if(!isset($result))
                    {
                        show_404();
                    }
                }
                
                $userInsert = array(
                    'userName'      => htmlspecialchars($this->input->post('userName')), 
                    'password'      => crypt($this->input->post('password')),
                    'name'          => htmlspecialchars($this->input->post('name')), 
                    'lastName'      => htmlspecialchars($this->input->post('lastName')), 
                    'turn'          => filter_var($this->input->post('turn'),FILTER_SANITIZE_FULL_SPECIAL_CHARS), 
                    'cuil'          => htmlspecialchars($this->input->post('cuil')), 
                    'createDate'    => date('Y-m-d H:i:s'),
                    'teamLeaderId'  => $this->input->post('teamLeader') == "EMPTY" ? NULL : htmlspecialchars($this->input->post('teamLeader')) , 
                    'roleId'        => $this->input->post('role') == "EMPTY" ? NULL : htmlspecialchars($this->input->post('role')) , 
                );
                $res = $this->Users->Create($userInsert);
                if(isset($res)){

                    if($this->input->post('user_campaigns')){

                        $userInCampaigns = array();
                        $userCampaign = new StdClass();
                        foreach ($this->input->post('user_campaigns') as $index => $campaignId) {

                            $userCampaign->userId     = $res;
                            $userCampaign->campaignId = $campaignId;
                            array_push($userInCampaigns, $userCampaign);
                            $userCampaign = new StdClass();
                        }
                        
                        $result = $this->Users->InsertUserCampaigns($userInCampaigns);
                        if($result == FALSE){
                            show_404();   
                        }
                    }

                    $this->session->set_flashdata('userMessage', 'create');
                    header('Location:/'.FOLDERADD.'/users');    
                }

            }

        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }
    //aca solo editar los usuarios menores
    public function edit()
    {
        if($this->uri->segment(3))
        {
            $userId = $this->uri->segment(3);
            $sql = "SELECT * FROM users WHERE userId = ? AND active = 1";
            $res = $this->db->query($sql, $userId)->row();

            if(isset($res))
            {
                if($this->Identity_model->Validate('users/admin')){
                    $editHierarchy = $this->Identity_model->getHierarchy($res->userId);
                    $ownHierarchy = $this->Identity_model->getHierarchy($this->session->UserId);
                    if($editHierarchy >= $ownHierarchy){
                    
                        $this->form_validation->set_rules('userName'        ,'lang:login_username'         ,'required|max_length[30]|edit_unique[users.userName.userId.'.$userId.'.TRUE]');
                        $this->form_validation->set_rules('name'            ,'lang:general_name'           ,'required|max_length[50]');
                        $this->form_validation->set_rules('lastName'        ,'lang:admin_users_lastName'   ,'required|max_length[50]');
                        $this->form_validation->set_rules('turn'            ,'lang:admin_users_turn'       ,'required|max_length[50]|callback_check_turn');
                        $this->form_validation->set_rules('user_campaigns[]', 'lang:main_campaigns'        ,'numeric|callback_campaign_exists|callback_unique_campaign[user_campaigns[]]');
                        $this->form_validation->set_rules('cuil'            ,'lang:admin_users_cuil'       ,'required|min_length[11]|max_length[11]|numeric');
                        $this->form_validation->set_rules('role'            ,'lang:admin_users_role'       ,'exist_role');

                        if ($this->form_validation->run() == FALSE) {
                            $data = $this->Users->GetUserData($userId);
                            
                            if($this->input->post('user_campaigns')){

                                $campaignIds = $this->input->post('user_campaigns');
                                
                                $this->db->select('campaignId, name');
                                $this->db->where_in('campaignId',$campaignIds);
                                
                                $data->user_campaigns = $this->db->get('campaigns')->result();
                            }

                            $this->load->view('_shared/header');
                            $this->load->view('users/edit',$data);
                            $this->load->view('_shared/footer');

                        } 
                        else 
                        {
                            //para validar si el id de teamleader enviado está en la base de datos
                            if($this->input->post('teamLeader') && $this->input->post('teamLeader') != "EMPTY")
                            {

                                $sql = "SELECT * FROM teamLeaders WHERE teamLeaderId = ?";
                                $result = $this->db->query($sql,$this->input->post('teamLeader'))->row();

                                if(!isset($result))
                                {
                                    show_404();
                                }
                            }

                            //para validar si el id de role enviado está en la base de datos
                            if($this->input->post('role') && $this->input->post('role') != "EMPTY")
                            {
                                $roleId = $this->input->post('role') ? $this->input->post('role') : $this->input->post('roleH');

                                $sql = "SELECT * FROM roles WHERE roleId = ?";
                                $result = $this->db->query($sql,$roleId)->row();
                                
                                if(!isset($result))
                                {
                                    show_404();
                                }
                            }

                            
                            $userEdit = array(
                                'userName'      => htmlspecialchars($this->input->post('userName')), 
                                'name'          => htmlspecialchars($this->input->post('name')), 
                                'lastName'      => htmlspecialchars($this->input->post('lastName')), 
                                'turn'          => htmlspecialchars($this->input->post('turn'),FILTER_SANITIZE_FULL_SPECIAL_CHARS), 
                                'cuil'          => htmlspecialchars($this->input->post('cuil')), 
                                'teamLeaderId'  => $this->input->post('teamLeader') == "EMPTY" ? NULL : htmlspecialchars($this->input->post('teamLeader')) , 
                                'roleId'        => $this->input->post('role') == "EMPTY" ? NULL : htmlspecialchars($this->input->post('role'))  
                            );
                            
                            $res = $this->Users->Edit($userId,$userEdit);
                            if($res == "success"){

                                if($this->input->post('user_campaigns')){

                                    $userInCampaigns = array();
                                    $userCampaign = new StdClass();
                                    foreach ($this->input->post('user_campaigns') as $index => $campaignId) {

                                        $userCampaign->userId     = $userId;
                                        $userCampaign->campaignId = $campaignId;
                                        array_push($userInCampaigns, $userCampaign);
                                        $userCampaign = new StdClass();
                                    }
                                    
                                    $result = $this->Users->InsertUserCampaigns($userInCampaigns);
                                    if($result == FALSE){
                                        show_404();   
                                    }
                                }
                                $this->session->set_flashdata('userMessage', 'edit');
                                header('Location:/'.FOLDERADD.'/users');    
                            }
                        }
                    }
                    else {
                        header('Location:/'.FOLDERADD.'/users');
                    }
                }
                else {
                    header('Location:/'.FOLDERADD);
                } 
            }
            else {
                header('Location:/'.FOLDERADD);
            } 

        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function check_turn()
    {
        $turns = getTurns();
        if(in_array($this->input->post('turn'),$turns))
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_turn',$this->lang->line('users_unexisted_turn'));
            return FALSE;
        }
    }
    //aca igual que en edit
    public function delete()
    {
        if($this->Identity_model->Validate('users/admin')){

            $this->getViewDetailsOrDelete("delete");
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function view()
    {
        if($this->Identity_model->Validate('users/view')){
            $this->getViewDetailsOrDelete("view");
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    private function getViewDetailsOrDelete($type)
    {
        if($this->uri->segment(3)){
            if($this->Identity_model->Validate('users/view')){
                $userId = $this->uri->segment(3);

                $res = new StdClass();
                $res->user = $this->Users->GetDetailDeleteData($userId);

                if(isset($res->user))
                {
                    $editHierarchy = $this->Identity_model->getHierarchy($userId);
                    $ownHierarchy = $this->Identity_model->getHierarchy($this->session->UserId);
                    if($editHierarchy >= $ownHierarchy){

                        if($this->session->UserId == $this->uri->segment(3) && $this->uri->segment(2) == "delete"){
                            $this->session->set_flashdata('deleteMessage', 'selfDelete');
                            header('Location:/'.FOLDERADD.'/users');
                        }
                        else{
                            if($this->input->post('userId') && $this->input->post('userId') == $res->user->userId && $type == 'delete'){
                                    
                                $userDelete = array('userId' => $res->user->userId);
                                $res = $this->Users->Delete($userDelete);
                                if($res == "success")
                                {
                                    $this->session->set_flashdata('userMessage', 'delete');
                                    header('Location:/'.FOLDERADD.'/users');
                                }
                            }
                            else {
                                $res->type = $type;
                                
                                $this->load->view('_shared/header');
                                $this->load->view('users/view-delete',$res);
                                $this->load->view('_shared/footer');
                            }
                        }
                    }
                    else {
                        header('Location:/'.FOLDERADD.'/users');
                    }    
                }
                else {
                    header('Location:/'.FOLDERADD);
                }
            }
            else {
                header('Location:/'.FOLDERADD);
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    /*public function config()
    {
        if($this->Identity_model->Validate('users/view')){
            $this->load->view('_shared/header');
            $this->load->view('users/config');
            $this->load->view('_shared/footer');

        }
        else{
            header('Location:'.base_url());
        }
    }*/


public function changePass()
{
    if($this->Identity_model->Validate('users/changepass'))
    {
        $this->form_validation->set_rules('password'          , 'lang:login_password'                ,'required|min_length[4]|max_length[128]');
        $this->form_validation->set_rules('confirmPassword'   , 'lang:admin_users_confirm_password'  , 'required|matches[password]',
            array(
                'matches'     => $this->lang->line('admin_users_passwordmatch')
            ));
        $this->form_validation->set_rules('oldpassword'       , 'lang:login_password'                ,'required|max_length[128]');

        if ($this->form_validation->run() == FALSE) 
        {
            $this->changePassView();
        } 
        else
        {
            $sql = 'SELECT userId, userName, password, roleId FROM users WHERE active = 1 AND userId = ?';
            $query = $this->db->query($sql, $this->session->UserId)->row();
            
            if (isset($query)) 
            {

                if($this->input->post('oldpassword'))
                {

                    if(hash_equals($query->password,crypt($this->input->post('oldpassword'),$query->password)))
                    {
                        $userEdit = array(
                            'password' => crypt($this->input->post('password'))
                        );

                        $res = $this->Users->EditPass($query->userId,$userEdit);
                    }
                    else
                    {
                        $data['invalidPassword'] = TRUE;
                        $this->changePassView($data);
                        return;  
                    }
                }
                else
                {
                    $userEdit = array(
                        'password' => crypt($this->input->post('password'))
                    );

                    $res = $this->Users->EditPass($query->userId,$userEdit);
                }

                if($res == "success")
                {
                    $this->session->set_flashdata('userMessage', 'edit');
                    header('Location:/'.FOLDERADD.'/users/changePass');    
                }
            }
            else
            {
                show_404();
            }
        }
    }
    else{
        header('Location:/'.FOLDERADD);
    }

}

public function resetpass()
{
    if($this->Identity_model->Validate('users/admin'))
    {
        
        $this->form_validation->set_rules('password'          , 'lang:login_password'                ,'required|min_length[4]|max_length[128]');
        $this->form_validation->set_rules('confirmPassword'   , 'lang:admin_users_confirm_password'  , 'required|matches[password]',
            array(
                'matches'     => $this->lang->line('admin_users_passwordmatch')
            ));
        $this->form_validation->set_rules('user'       , 'lang:login_username'                ,'required|max_length[50]');

        if ($this->form_validation->run() == FALSE) 
        {
            $this->resetPassView();
        } 
        else
        {
            
            $sql = 'SELECT u.userId, userName, password, r.roleId, hierarchyId
            FROM users u
            LEFT JOIN roles r
            ON u.roleId = r.roleId
            WHERE userName = ?';
            $query = $this->db->query($sql, $this->input->post('user'))->row();
            
            if (isset($query)) 
            {
                $ownHierarchy = $this->Identity_model->getHierarchy($this->session->UserId);

                if($query->hierarchyId > $ownHierarchy){
                    $userEdit = array(
                        'password' => crypt($this->input->post('password'))
                    );
    
                    $res = $this->Users->EditPass($query->userId,$userEdit);
    
                    if($res == "success")
                    {
                        $this->session->set_flashdata('userMessage', 'edit');
                        header('Location:/'.FOLDERADD.'/users/resetPass');    
                    }
                }
                else
                {
                    $data['userNotRightToEdit'] = TRUE;
                    $this->resetPassView($data);
                }
            }
            else
            {
                $data['userNotExists'] = TRUE;
                $this->resetPassView($data);
            }
        }
    }
    else{
        header('Location:/'.FOLDERADD);
    }


}

private function changePassView($data = NULL)
{
    $this->load->view('_shared/header');
    $this->load->view('users/changepass',$data);
    $this->load->view('_shared/footer');
}

private function resetPassView($data = NULL)
{
    $this->load->view('_shared/header');
    $this->load->view('users/resetpass',$data);
    $this->load->view('_shared/footer');
}

// public function search_users()
// {
//     if($this->Identity_model->Validate('campaigns/admin') && $this->input->post('search')){
//         $rules = array(array(
//             'field' => 'search',
//             'label' => 'lang:user_search_field',
//             'rules' => 'required'));
    
//         $this->form_validation->set_rules($rules);
    
//         if(! $this->form_validation->run())
//         {
//             echo json_encode(array());
//             return;
//         }
    
//         $searchValue = strtolower(substr(trim($this->input->post('search', TRUE)), 0, 30));
//         echo json_encode($this->Users->SearchUsers($searchValue));
//     }
//     else {
//         show_404();
//     }
// }
    public function campaign_exists($campaign)
    {
        $response = TRUE;
        if(strlen($campaign) > 0)
        {
            $this->db->where('campaignId', $campaign);
            $this->db->where('active', 1);
            $this->db->from('campaigns');

            if($this->db->count_all_results() == 0)
            {
                $this->form_validation->set_message('campaign_exists', $this->lang->line('campaign_error_campaign_not_exist'));
                $response = FALSE;
            }
        }

        return $response;
    }

    public function unique_campaign($campaign, $field_name)
    {
        $campaigns_list = $this->input->post($field_name);
        $campaignsCount = count($campaigns_list);
        $campaignEncounters = 0;

        for($i = 0; $i < $campaignsCount; $i++)
        {
            if($campaign == $campaigns_list[$i])
            {
                $campaignEncounters++;
                if($campaignEncounters >= 2)
                    break;
            }
        }

        if($campaignEncounters > 1)
        {
            $this->form_validation->set_message('unique_campaign', $this->lang->line('campaign_error_campaign_in_list'));
            return FALSE;
        }

        return TRUE;
    }

}

?>
