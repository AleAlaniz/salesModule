<?php 
if(!defined('BASEPATH')) exit('No direct scrript access allowed');

if(!function_exists('asset_url()'))
{
	function asset_url()
	{
		return base_url('assets/');
	}

	function getTurns()
	{
		$turns = array('Mañana','Tarde','Noche','Tiempo completo');
		return $turns;
	}
}
