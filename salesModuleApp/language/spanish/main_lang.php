<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login_username']                              = 'Nombre de Usuario';
$lang['login_password']                              = 'Contrase&ntilde;a';
$lang['login_error_message']                         = 'El nombre de usuario y la contrase&ntilde;a no coinciden';
$lang['login_signin'] 		                         = 'Entrar';
$lang['user_search_field'] 						     = 'Búsqueda';
$lang['log_out']                                     = 'Salir';

//general
$lang['main_log_out']                                = 'Salir';
$lang['main_my_profile']                             = 'Mi perfil';
$lang['main_users']                                  = 'Usuarios';
$lang['main_teamleaders']                            = 'Team Leaders';
$lang['main_campaigns']                       	     = 'Campañas';
$lang['main_product']                                = 'Producto';
$lang['main_hierarchy']                              = 'Jerarquías';
$lang['general_create']                              = 'Crear';
$lang['general_delete']                              = 'Eliminar';
$lang['general_edit']                                = 'Editar';
$lang['general_accept']                              = 'Aceptar';
$lang['general_cancel']                              = 'Cancelar';
$lang['general_save']                                = 'Guardar';
$lang['general_goback']                              = 'Atrás';
$lang['general_details']                             = 'Detalles';
$lang['general_sales']                               = 'Ventas';
$lang['general_reports']                             = 'Reportes';
$lang['general_campaign']  		                     = 'Campaña';
$lang['general_search']  		                     = 'Buscar';
$lang['general_name']                      		     = 'Nombre';
$lang['general_undefined']                           = 'No definido';
$lang['general_completeall']                         = 'Complete todos los campos marcados con';
$lang['general_empty_rows']                    	     = 'No hay datos disponibles.';

$lang['general_yes']                                 = 'Si';
$lang['general_no']                                  = 'No';

//roles
$lang['main_roles'] 		                         = 'Perfiles';
$lang['role_create']  		                         = 'Crear Perfil';
$lang['role_permissions']  	                         = 'Permisos';
$lang['general_edit_title'] 	                     = 'Editar perfil';
$lang['general_delete_title'] 	                     = 'Eliminar perfil';
$lang['role_view_title'] 	                         = 'Detalles del perfil';
$lang['role_delete_confirm']                 	     = '¿Está seguro que desea eliminar este perfil?';
$lang['role_create_success']                         = 'El perfil ha sido creado correctamente.';
$lang['general_edit_success'] 	                     = 'El perfil ha sido editado correctamente.';
$lang['general_delete_success']                      = 'El perfil ha sido eliminado correctamente.';
$lang['no_permissions']                 		     = 'El perfil aún no tiene permisos asignados.';

//users
$lang['admin_users_lastName']                        = 'Apellido';
$lang['admin_users_turn']                            = 'Turno';
$lang['admin_users_campaign'] 						 = 'Campañas asignadas';
$lang['admin_users_teamleader']                      = 'Team Leader';
$lang['admin_users_hierarchy']                       = 'Jerarquía';
$lang['admin_users_cuil']                            = 'Cuil';
$lang['admin_users_role']                            = 'Perfil';
$lang['admin_users_changepass']                      = 'Cambiar contraseña';
$lang['admin_users_resetpass']                       = 'Resetear contraseña';
$lang['admin_users_user_name']                       = 'Nombre del usuario';
$lang['admin_users_new_password']                    = 'Nueva contraseña';
$lang['admin_users_confirm_password']                = 'Confirmar contraseña';
$lang['admin_users_create']                          = 'Crear usuario';
$lang['admin_users_edit']                            = 'Editar usuario';
$lang['admin_users_delete']                          = 'Eliminar usuario';
$lang['admin_users_passwordmatch']                   = 'Las contraseñas no coinciden';
$lang['admin_users_successmessage']                  = 'Usuario creado correctamente';
$lang['admin_users_editmessage']                     = 'Usuario editado correctamente';
$lang['admin_users_deletemessage']                   = 'Usuario eliminado correctamente';
$lang['admin_users_details']                         = 'Detalles del usuario';
$lang['admin_users_config']                          = 'Configuración';
$lang['admin_users_resetPass']                       = 'Resetear contraseña';
$lang['admin_areyousure_delete_user']                = '¿Está seguro que desea eliminar a este usuario?';
$lang['admin_users_invalidPassword']                 = 'La contraseña ingresada debe ser la misma a la anterior';
$lang['admin_users_userNotExists']            	     = 'El usuario ingresado no existe';
$lang['admin_users_selfDelete']                      = 'No puedes eliminarte a ti mismo';
$lang['users_empty']            				     = 'No hay usuarios';
$lang['users_unexisted_turn']                        = 'No existe el turno seleccionado';
$lang['admin_users_userNotRightToEdit']        	     = 'No tiene permiso para editar la contraseña del usuario seleccionado';

//admin teamleaders
$lang['admin_teamleaders_create']                    = 'Crear Team Leader';
$lang['admin_teamleaders_edit']                      = 'Editar Team Leader';
$lang['admin_teamleaders_delete']                    = 'Eliminar Team Leader';
$lang['admin_teamleaders_successmessage']            = 'Team Leader creado correctamente';
$lang['admin_teamleaders_editmessage']               = 'Team Leader editado correctamente';
$lang['admin_teamleaders_deletemessage']             = 'Team Leader eliminado correctamente';
$lang['admin_teamleaders_areyousure_delete']         = '¿Está seguro que desea eliminar a este Team Leader?';
$lang['admin_teamleaders_details']                   = 'Detalles del Team Leader';


//Campañas

$lang['campaign_campaings'] 						 = 'Campañas';
$lang['campaign_name'] 								 = 'Nombre';
$lang['campaign_description'] 						 = 'Descripción';
$lang['campaign_users'] 							 = 'Usuarios de la campaña';
$lang['campaign_users_added'] 						 = 'Usuarios añadidos';
$lang['campaign_id'] 								 = 'Identificador de campaña';
$lang['campaign_users_count'] 						 = 'Cantidad de usuarios';
$lang['campaign_products_count'] 					 = 'Cantidad de productos';

$lang['campaign_create'] 							 = 'Crear campaña';
$lang['campaign_edit'] 								 = 'Editar campaña';
$lang['campaign_details'] 							 = 'Detalles de campaña';
$lang['campaign_delete'] 							 = 'Eliminar de campaña';

$lang['campaign_del'] 							     = 'Eliminar campaña';


$lang['campaign_create_success'] 					 = 'Campaña creada correctamente.';
$lang['campaign_edit_success']	 					 = 'Campaña editada correctamente.';
$lang['campaign_delete_success'] 					 = 'Campaña eliminda correctamente.';

$lang['campaign_delete_alert'] 						 = '¿Estás seguro que deseas eliminar esta campaña?';
		 
$lang['campaign_error_user_not_exist'] 				 = 'Uno de los usuarios seleccionados no existe.';
$lang['campaign_error_campaign_not_exist'] 			 = 'La campaña a la que hace referencia no está disponible.';
$lang['product_error_product_not_exist'] 			 = 'El producto al que hace referencia no está disponible.';
$lang['campaign_error_user_exist_in_list'] 			 = 'No puede haber usuarios repetidos en la campaña.';
$lang['campaign_error_campaign_not_exist'] 			 = 'Una de las campañas seleccionadas no existe.';
$lang['campaign_error_campaign_in_list'] 		 	 = 'Un usuario no puede estar asignado mas de una vez a la misma campaña.';
$lang['hierarchy_error_hierarchy_not_exists']        = 'La jerarquía a la que hace referencia no existe';
$lang['role_error_role_not_exists']                  = 'El rol al que hace referencia no existe';

$lang['general_database_error'] 					 = 'Ocurrió un error inesperado.';

$lang['user_complete_name'] 						 = 'Nombre completo';
$lang['campaign_products'] 							 = 'Ver productos de la campaña';

$lang['campaign_products'] 		               		 = 'Productos de la campaña';
$lang['product_create_title']  		           		 = 'Crear producto';
$lang['product_edit_title'] 	               		 = 'Editar producto';
$lang['product_delete_title'] 	               		 = 'Eliminar producto';
$lang['product_name'] 	            				 = 'Nombre de producto';
$lang['product_detail'] 	               			 = 'Detalle de producto';
$lang['product_no_name'] 	               			 = 'Debe ingresar el nombre del producto';
$lang['campaignProducts'] 	               			 = 'Productos de campaña';
$lang['product_name_too_long'] 	               		 = 'El nombre del producto es muy largo';
$lang['product_no_detail'] 	               			 = 'Debe ingresar el nombre del producto';
$lang['product_detail_too_long'] 	           		 = 'El nombre del producto es muy largo';
$lang['campaign_error_product_name_unique_in_list']  = 'El nombre de cada producto debe ser único';
$lang['campaign_error_product_required']      		 = 'Debe ingresar los datos requeridos';
$lang['campaign_added_products']      		 		 = 'Productos añadidos';
$lang['products_empty']      		 		 		 = 'No hay productos';
$lang['product_input_only_characters']      		 = 'Solamente se aceptan letras y espacios como caracteres validos';
$lang['campaing_extra_data']                         = 'Datos extra';
$lang['campaign_extra_data_required']                = 'Datos extra obligatorios';

//Ventas
$lang['sales_salesDate']                             = 'Fecha de venta';
$lang['sales_clientNumber']                          = 'Número de cliente';
$lang['sales_contractNumber']                        = 'Número de contrato';
$lang['sales_comment']                               = 'Comentario';
$lang['sales_phone']                                 = 'Número de teléfono';
$lang['sales_dni']                                   = 'Dni';
$lang['sales_create']                                = 'Crear venta';
$lang['sales_campaign']                              = 'Campaña';
$lang['sales_census']                                = 'Id de empadronamiento';
$lang['sales_commercial_offer']                      = 'Oferta comercial';
$lang['report_team_leader']                          = 'Team leader';
$lang['report_comment']                          	 = 'Comentario';
$lang['report_phone']                          		 = 'Teléfono';
$lang['report_dni']                          		 = 'Dni';
$lang['census_id']                                   = 'Id de empadronamiento';
$lang['commercial_offer']                            = 'Oferta comercial';
$lang['sale_edit']                          		 = 'Editar venta';
$lang['sale_delete']                          		 = 'Eliminar venta';
$lang['sale_id']                                     = 'Identificador de ventas';

$lang['sales_createmessage']                         = 'Venta correctamente cargada';
$lang['sales_want_to_resale']                        = '¿Desea cargar otra venta para el mismo cliente?';
$lang['sales_not_permission']                        = 'No tienes permiso para acceder a esta vista.';

//Reportes
$lang['report_sale_reports'] 	                     = 'Reporte de ventas';
$lang['report_contract_number']                      = 'Nro de contrato';
$lang['report_username']							 = 'Nombre de usuario';
$lang['report_agent']			                     = 'Operador';
$lang['report_campaign'] 		                     = 'Campaña';
$lang['report_product'] 		                     = 'Producto';
$lang['report_sale_date'] 		                     = 'Fecha';
$lang['report_sale_hour'] 		                 	 = 'Hora';
$lang['report_sale_time_date'] 		                 = 'Fecha y hora';

$lang['report_sales'] 			                     = 'Ventas';
$lang['report_start_date'] 		                     = 'Fecha de inicio';
$lang['report_end_date'] 		                     = 'Fecha de fin';
$lang['report_export'] 			                     = 'Exportar';

$lang['report_date_greater_error'] 	                 = 'La fecha de inicio no puede ser mayor a la fecha final.';
$lang['date_invalid'] 				                 = 'Una de las fechas indicadas no es correcta.';
$lang['date_format_error'] 				             = 'El formato del campo fecha es incorrecto.';
$lang['hour_format_error'] 				             = 'El formato del campo hora es incorrecto.';
$lang['sale_invalid'] 				             	 = 'La venta que intenta modificar no existe.';
$lang['sale'] 				             	 		 = 'Venta';





$lang['hierarchy_edit'] 	                     	 = 'Editar jerarquía';
$lang['hierarchy_position'] 	                     = 'Posición en la jerarquía';
$lang['hierarchy_position_first'] 	                 = 'Nivel más alto';
$lang['hierarchy_position_after'] 	                 = 'Debajo de';
$lang['hierarchy_position_last'] 	                 = 'Nivel más bajo';


//Jerarquía
$lang['admin_hierarchy_create']                    = 'Crear Jerarquía';
$lang['admin_hierarchy_edit']                      = 'Editar Jerarquía';
$lang['admin_hierarchy_delete']                    = 'Eliminar Jerarquía';
$lang['admin_hierarchy_successmessage']            = 'Jerarquía creada correctamente';
$lang['admin_hierarchy_editmessage']               = 'Jerarquía editada correctamente';
$lang['admin_hierarchy_deletemessage']             = 'Jerarquía eliminada correctamente';
$lang['admin_hierarchy_areyousure_delete']         = '¿Está seguro que desea eliminar a esta Jerarquía?';
$lang['admin_hierarchy_under']                     = 'La jerarquía estará debajo de';
$lang['admin_hierarchy_level']                     = 'Nivel de jerarquía';
$lang['admin_hierarchy_areyousure_delete']         = '¿Está seguro que desea eliminar este nivel de jerarquía?';
$lang['admin_hierarchy_details']         		   = 'Detalles del nivel de jerarquía';
$lang['admin_hierarchy_no_permission']        	   = 'No se puede modificar la posicion de este nivel';
$lang['admin_hierarchy_same_level']        	   	   = 'Dejar al mismo nivel';
$lang['admin_hierarchy_change_level']        	   = 'Mover debajo de';
$lang['admin_hierarchy_change_level']        	   = 'Mover debajo de';
$lang['admin_hierarchy_delete_error']        	   = 'Error al eliminar nivel de jerarquía';

$lang['admin_hierarchy_delete_error_text']        	   = 'No es posible eliminar el nivel de jerarquía actual ya que fue asignado a los siguientes roles:';

$lang['max_length_text'] 						    = '*Maximo 500 caracteres';

$lang['report_sale_areyousure_delete']				= '¿Estas seguro que desea eliminar esta venta?';
$lang['report_sales_deletemessage']                 = 'Venta correctamente eliminada';
$lang['report_sales_editmessage']                 = 'La venta fue editada de manera exitosa';