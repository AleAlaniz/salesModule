<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_tries {

	public function is_blocked($login_try_row)
	{
		if($login_try_row->nAttempts == 3)
			return true;

		return false; 
	}

	public function clean_tries($login_try_row)
	{
		$login_try_row->nAttempts = 0;
		$login_try_row->timestamp = date('y-m-d h:i:s', strtotime("+10 minutes"));
		return $login_try_row;
	}

	public function get_tries($login_try_row)
	{

		return $login_try_row->nAttempts;
	}

	public function cooldown_over($login_try_row)
	{

		$timestamp = date('y-m-d h:i:s');

		$login_phpdate = strtotime( $login_try_row->timestamp );
		$login_mysqldate = date( 'y-m-d h:i:s', $login_phpdate );

		if ($timestamp > $login_mysqldate)
			return true;

		return false;
	}

	public function add_try($login_try_row)
	{
		$login_try_row->nAttempts += 1;

		return $login_try_row;
	}

}
?>