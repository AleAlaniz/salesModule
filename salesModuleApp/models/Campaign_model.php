<?php 
Class Campaign_model extends CI_Model
{
    public function GetCampaigns()
    {
        $sql = 'SELECT c.campaignId, c.name, COUNT(u.userId) as "userCount", pc.productsCount
        FROM campaigns c 
        LEFT JOIN usersInCampaigns uc ON c.campaignId = uc.campaignId
        LEFT JOIN (select userId from users where active = 1) u ON uc.userId = u.userId
        INNER JOIN 
        (SELECT c.campaignId, COUNT(p.productId) as "productsCount"
        FROM campaigns c
        LEFT JOIN (SELECT campaignId,productId FROM products where active = 1) p
        ON p.campaignId = c.campaignId
        WHERE c.active = 1
        GROUP BY c.campaignId, c.name) pc
        ON pc.campaignId = c.campaignId
        WHERE c.active = 1
        GROUP BY c.campaignId, c.name';

        return $this->db->query($sql)->result();
    }
    
    public function GetCampaignsWithProducts()
    {
        $data = new StdClass();

        $sql = "SELECT productId, name, campaignId
        FROM products
        WHERE active = 1 AND campaignId = ?";

        $data->products = $this->db->query($sql, $this->session->selectedCampaign)->result();
        foreach ($data->products as $product) {
            $product->name = str_replace("'",chr(39),$product->name);
            $product->name = str_replace('"',chr(34),$product->name);
        }
        $data->products = $data->products;

        $sql = "SELECT c.extra_data, c.extra_data_required
        FROM campaigns c
        WHERE active = 1 AND campaignId = ?";

        $data->campaign_extra_data = $this->db->query($sql, $this->session->selectedCampaign)->row();
        
        return $data; 
    }

    public function GetCampaign($campaign_id)
    {
        $results = array('campaign' => null, 'users' => array());

        $sql = 'SELECT campaignId, name, description, extra_data, extra_data_required
        FROM campaigns
        WHERE campaignId = ? AND active = 1
        LIMIT 1';

        $result['campaign'] = $this->db->query($sql, $campaign_id)->row();

        $sql = 'SELECT u.userId, CONCAT(u.name, " ", u.lastName) as "completeName", turn
        FROM users u JOIN usersInCampaigns uc
        ON u.userId = uc.userId
        WHERE uc.campaignId = ? AND active = 1';

        $result['users'] = $this->db->query($sql, $campaign_id)->result();

        $sql = "SELECT prod.productId,prod.name,prod.detail
        FROM products prod
        WHERE campaignId = ?
        AND active = 1";

        $result['products'] = $this->db->query($sql, $campaign_id)->result();
        
        return $result; 
    }

    public function Create()
    {
        $sql = 'INSERT INTO campaigns VALUES (null, ?, ?, ?, ?, 1)';
        $result = $this->db->query($sql, array(
            $this->input->post('campaign_name', TRUE),
            $this->input->post('campaign_description', TRUE),
            $this->input->post('campaign_extra_data_h'),
            $this->input->post('campaign_extra_data_required_h')
        ));

        if($result == FALSE)
            return null;

        $campaign_id = $this->db->insert_id();

        $productsToInsert = array();
        $productsCount = count($this->input->post('campaignProducts'));

        if ($productsCount > 0) {

            foreach ($this->input->post('campaignProducts') as $key => $product) {

                array_push($productsToInsert, array(
                    'name'          => htmlspecialchars($product['name']),
                    'detail'        => htmlspecialchars($product['detail']),
                    'campaignId'    => $campaign_id,
                    'active'        => 1,
                    )
                );
            }


            $result = $this->db->insert_batch('products', $productsToInsert);
        }

        return ($result == FALSE) ? null : $campaign_id;
    }

    public function Edit()
    {
        $sql = 'UPDATE campaigns
        SET name = ?, description = ?, extra_data = ?, extra_data_required = ?
        WHERE campaignId = ?';

        $campaign_id = $this->input->post('campaign_id');
        
        $result = $this->db->query($sql, array(
            $this->input->post('campaign_name', TRUE),
            $this->input->post('campaign_description', TRUE),
            $this->input->post('campaign_extra_data_h', TRUE),
            $this->input->post('campaign_extra_data_required_h', TRUE),
            $campaign_id
        ));

        if($result == FALSE)
            return null;

        $productsToEdit = $this->input->post('campaignProducts');
        $productsCount  = sizeof($productsToEdit);

        $sql="UPDATE products
        SET active = 0
        WHERE campaignId = ?";
        $result = $this->db->query($sql,$campaign_id);

        if($result == FALSE)
            return null;

        if ($productsCount > 0) {

            for ($i=0; $i < $productsCount; $i++) { 

                $productsToEdit[$i]["campaignId"] = $campaign_id;

                if (isset($productsToEdit[$i]['productId'])){

                    $productsToEdit[$i]['name'] = htmlspecialchars($productsToEdit[$i]['name']);
                    $productsToEdit[$i]['detail'] = htmlspecialchars($productsToEdit[$i]['detail']);
                    
                    $sql= "UPDATE products
                    SET active = 1, name = ?, detail = ?
                    WHERE campaignId = ? AND productId = ?";
                    $result = $this->db->query($sql, array($productsToEdit[$i]['name'],$productsToEdit[$i]['detail'],$campaign_id,$productsToEdit[$i]['productId']));
                }
                else{
                    
                    $productsToEdit[$i]['name'] = htmlspecialchars($productsToEdit[$i]['name']);
                    $productsToEdit[$i]['detail'] = htmlspecialchars($productsToEdit[$i]['detail']);
                    $productsToEdit[$i]['campaignId'] = $campaign_id;
                    $productsToEdit[$i]['active'] = 1;
                    
                    $result = $this->db->insert('products', $productsToEdit[$i]);
                }

                if($result == FALSE)
                    break;
            }
        }
        return ($result == FALSE) ? null : $campaign_id;        
    }

    public function Delete()
    {

        $sql="UPDATE products
        SET active = 0
        WHERE campaignId = ?";
        $deleteProductQuery = $this->db->query($sql,$this->input->post('campaign_id'));

        $sql = "UPDATE campaigns
        SET active = 0
        WHERE campaignId = ?";
        $deleteCampaignQuery = $this->db->query($sql, $this->input->post('campaign_id'));

        return ($deleteProductQuery && $deleteCampaignQuery);
    }

    public function GetCampaignsForm()
    {
        $sql = "SELECT campaignId, name
        FROM campaigns";

        return $this->db->query($sql)->result();
    }

    public function GetMyCampaigns()
    {
        $sql = 'SELECT c.campaignId, c.name
        FROM campaigns c join usersInCampaigns uc
        ON(c.campaignId = uc.campaignId)
        WHERE uc.userId = ? AND c.active = 1';

        return $this->db->query($sql, $this->session->UserId)->result();
    }

    public function SearchCampaigns($searchString)
    {
        $sql = '
        SELECT c.name, c.campaignId
        FROM campaigns c 
        WHERE c.active = 1 
        AND LOWER(c.name) LIKE ?
        ORDER BY c.name
        LIMIT 7';

        return $this->db->query($sql,'%'.$searchString.'%')->result();
    }
}

?>