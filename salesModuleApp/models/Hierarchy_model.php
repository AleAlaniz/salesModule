<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hierarchy_model extends CI_Model {

    
    public function __construct()
    {
        parent::__construct();

    }
    
    public function GetHierarchys()
    {
        $hierarchyId = $this->Identity_model->getHierarchy($this->session->UserId);
        $sql = "SELECT hierarchyId,name FROM hierarchy WHERE hierarchyId >= ? ORDER BY hierarchyId ASC";
        $hiearchys = $this->db->query($sql,$hierarchyId)->result();
        return $hiearchys;
    }

    public function GetHierarchysEdit($urlid)
    {
        $hierarchyId = $this->Identity_model->getHierarchy($this->session->UserId);
        $sql = "SELECT hierarchyId,name FROM hierarchy WHERE hierarchyId >= ? AND hierarchyId <> ? ORDER BY hierarchyId ASC";
        $hiearchys = $this->db->query($sql,array($hierarchyId,$urlid))->result();
        return $hiearchys;
    }

    public function GetHierarchy($hierarchyId)
    {
        $sql = "SELECT hierarchyId,name FROM hierarchy WHERE hierarchyId = ? LIMIT 1";
        $hiearchy = $this->db->query($sql,$hierarchyId)->row();
        return $hiearchy;
    }

    public function GetHierarchiesUnder($hiearchyId)
    {
        $sql = "SELECT hierarchyId,name FROM hierarchy WHERE hierarchyId > ? ORDER BY hierarchyId DESC";
        $hiearchys = $this->db->query($sql,$hiearchyId)->result();
        return $hiearchys;
    }

    public function GetHierarchiesUnderEdit($hiearchyId,$urlId)
    {
        $sql = "SELECT hierarchyId,name FROM hierarchy WHERE hierarchyId > ? AND hierarchyId <> ? ORDER BY hierarchyId DESC";
        $hiearchys = $this->db->query($sql,array($hiearchyId,$urlId))->result();
        return $hiearchys;
    }

    public function Create($hierarchy)
    {
        if(isset($hierarchy))
        {
            $this->db->insert('hierarchy', $hierarchy);
            return "success";
        }
        return "error";
    }

    public function UpdateId($hUpdate,$hierarchyId)
    {
        if(isset($hUpdate))
        {   
            $this->db->where('hierarchyId',$hierarchyId);
            $this->db->update('hierarchy', $hUpdate);
            return "success";
        }
        return "error";
    }

    public function hierarchyExists($hierarchyId)
    {
        $sql = "SELECT hierarchyId,name FROM hierarchy WHERE hierarchyId = ? LIMIT 1";
        $query = $this->db->query($sql,$hierarchyId)->row();
        
        return isset($query);
    }

    public function deleteHierarchy($hierarchyId)
    {
        if (isset($hierarchyId)) {

            $this->db->where('hierarchyId',$hierarchyId);
            $this->db->delete('hierarchy');
            return "success";
        }
        return "error";
    }

    public function getHierarchyRoles($hierarchyId)
    {
        $sql = "SELECT r.name FROM roles r WHERE r.hierarchyId = ?";
        return $this->db->query($sql,$hierarchyId)->result();
    }

}
/* End of file Hierarchy_model.php */
?>
