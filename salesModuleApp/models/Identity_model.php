<?php 
Class Identity_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->library('Login_tries');
  }


  function Autenticate()
  { 
    if ($this->input->post('UserName') && $this->input->post('Password')) {

      $sql = "SELECT userId, name, lastName, roleId, password FROM users WHERE userName = ? AND active = 1 LIMIT 1";
      $query_user = $this->db->query($sql, $this->input->post('UserName'))->row();
      if (isset($query_user)) {

        //como primer paso, luego de obtener solamente el nbre usuario, validara los intentos por IP, los busca en la tabla

        $ip_address = $this->input->ip_address();

        $sql    = "SELECT * FROM login_tries lt WHERE lt.IP = ? LIMIT 1";
        $query_IP = $this->db->query($sql, $ip_address)->row();

        //si no lo encuentra, crea un objeto nuevo
        $ip_login_row = (isset($query_IP))?  $query_IP : (object) array(
          'IP' => $ip_address,
          'nAttempts' => 0,
          'timestamp' => date('y-m-d h:i:s', strtotime("+10 minutes")),
        );

        $ip_new = (isset($query_IP))?  false : true;


        $is_blocked = false;
        

        //el usuario esta bloqueado? Es decir, paso todos los intentos y respeto el cooldown?
        if ($this->login_tries->is_blocked($ip_login_row)) {

          $is_blocked = true;

          if ($this->login_tries->cooldown_over($ip_login_row)){

            $ip_login_row   = $this->login_tries->clean_tries($ip_login_row);

            $is_blocked   = false;
          }
        }

        if (!$is_blocked){
          //una vez se ha comprobado que no esta bloqueado, se va a la prueba de la contraseña

          //en caso de que el test haya pasado, todo funciona como deberia funcionar actualmente
          if (hash_equals($query_user->password,crypt($this->input->post('Password'), $query_user->password))){

            //primero borra la entrada de la IP, para prevenir que no funke despues
            $this->db->where('IP', $ip_address);
            $this->db->delete('login_tries');

           //$sql = "SELECT name FROM rolePermissions rp
           //JOIN permissons p
           //ON (rp.permissionId = p.permisonId)
           //WHERE p.active = 'TRUE'
           //AND rp.value = 'TRUE'
           //AND rp.roleId = ?";

           //$data = $this->db->query($sql,$query_user->roleId)->result();
           //$permissions = array();
           //foreach ($data as $permis) {
           //  $permissions [] = $permis->name;
           //}
            $sessionData = array(
             'Loged'       => TRUE, 
             'UserId'      => $query_user->userId,
             'fullName'    => $query_user->name.' '.$query_user->lastName,
             'RoleId'      => $query_user->roleId
           );

            $this->session->set_userdata($sessionData);
            $this->SetSelectedCampaign();
            return 'success';

          }
          //fallo! 
          else{

            $ip_login_row = $this->login_tries->add_try($ip_login_row);

            if($ip_new){
              $this->db->insert('login_tries', $ip_login_row);
            }
            else{

              $this->db->where('IP', $ip_address);
              $this->db->update('login_tries', $ip_login_row);
            }
            
            return $this->lang->line("login_error_message");

          }
        }
        else{
          return 'Has excedido el número de intentos disponible. Por favor intentá más tarde.';
        }
      }
      else {
        return 'El nombre de usuario no existe.';
      }
    }
    else {
      echo "entro por 404";
      show_404();
    }
  }

  public function Validate($permissionName)
  {
    if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
    {
     $validateRole = FALSE;

     $sql = 'SELECT u.userId
     FROM (SELECT userId, roleId FROM users WHERE userId = ? AND active = 1) u JOIN roles r
     ON(u.roleId = r.roleId)
     JOIN rolePermissions rp
     ON(r.roleId = rp.roleId)
     JOIN permissions p
     ON(rp.permissionId = p.permissionId)
     WHERE p.name = ?';

     $validateRole = $this->db->query($sql, array($this->session->UserId, $permissionName))->row();
     return isset($validateRole);
   }
   else
   {
     return FALSE;
   }
 }

 public function getHierarchy($userId)
 {
  if($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId')){

    $sql = "SELECT r.hierarchyId as hierarchyPosition
    FROM (
    SELECT roleId from users where userId = ? AND active = 1) user
    INNER JOIN roles r ON user.roleId = r.roleId";

    return $this->db->query($sql,$userId)->row()->hierarchyPosition;
  }
}

public function Logout()
{
  $this->session->sess_destroy();
  header('Location:/'.FOLDERADD.'/');
}

public function SetSelectedCampaign()
{
  $sql = 'SELECT c.campaignId
  FROM campaigns c JOIN usersInCampaigns uc
  ON(c.campaignId = uc.campaignId)
  WHERE uc.userId = ? AND c.active = 1
  LIMIT 1';

  $campaign_result = $this->db->query($sql,  $this->session->UserId )->row();

  $this->session->selectedCampaign = (isset($campaign_result)) ? $campaign_result->campaignId : null;
}
}