<?php 
Class Product_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function getCampaignProducts($campaignId)
    {
    	return $this->db->query("SELECT p.productId, p.name, p.campaignId FROM products p WHERE campaignId = ? AND active = 1",$campaignId)->result();
    }
    
    public function GetProductsById($campaignId)
    {
        $sql = "SELECT * FROM products WHERE campaignId = ? AND active = 1";
        $res = $this->db->query($sql,$campaignId)->result();
        return $res;
    }
}

?>