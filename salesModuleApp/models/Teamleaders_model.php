<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class TeamLeaders_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getTLs()
        {
            $sql = "SELECT teamleaderId,name FROM teamLeaders WHERE active = 1";
            $tls = $this->db->query($sql)->result();

            return $tls;
        }

        public function getTeamLeader($teamleaderId)
        {
            $sql = "SELECT * FROM teamLeaders WHERE teamLeaderId = ? AND active = 1";
            $res = $this->db->query($sql,$teamleaderId)->row();
            return $res;
        }
        
        function Create($teamLeader)
        {
            if(isset($teamLeader))
            {
                $this->db->insert('teamLeaders', $teamLeader);
                return "success";
            }
            return "error";
        }

        public function Edit($teamLeader)
        {
            if(isset($teamLeader))
            {
                $this->db->where('teamLeaderId', $teamLeader['teamLeaderId']);
                $this->db->update('teamLeaders',$teamLeader);
                return "success";
            }
            return "error";
        }

        public function Delete($teamLeader)
        {
            if(isset($teamLeader))
            {
                $this->db->where('teamLeaderId', $teamLeader['teamLeaderId']);
                $this->db->update('teamLeaders', array('active' => 0));
                return "success";
            }
            return "error";
        }

    }

    /* End of file TeamLeaders_model.php */
?>