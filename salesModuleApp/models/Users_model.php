<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function GetUsers()
    {
        $sql = "SELECT u.userId,userName,password, u.name, lastName, turn, cuil, createDate, t.name as teamLeader, cuil, r.name role
        FROM users u
        LEFT JOIN roles r
        ON (u.roleId = r.roleId)
        LEFT JOIN teamLeaders t
        ON (u.teamLeaderId = t.teamLeaderId)
        WHERE u.active = 1
        AND (r.hierarchyId > ?
        OR u.userId = ?)";

        $users = $this->db->query($sql,array($this->Identity_model->getHierarchy($this->session->UserId),$this->session->UserId))->result();
        return $users;
    }

    public function Create($user)
    {
        if(isset($user))
        {
            $this->db->insert('users', $user);
            return $this->db->insert_id();
        }
        return null;
    }

    public function getFormData()
    {
        $data = new StdClass();
        
        $hierarchyId = $this->Identity_model->getHierarchy($this->session->UserId);
        $sql = "SELECT roleId,name FROM roles WHERE hierarchyId >= ?";
        $data->roles = $this->db->query($sql,$hierarchyId)->result();
        
        $sql = "SELECT teamLeaderId,name FROM teamLeaders";
        $data->teamLeaders = $this->db->query($sql)->result();
        
        $data->turns = getTurns();

        return $data;
    }

    public function Edit($userId,$user)
    {
        if(isset($user))
        {
            $this->db->where('userId', $userId);
            $this->db->update('users',$user);

            $this->db->where('userId', $userId);
            $this->db->delete('usersInCampaigns');
            return "success";
        }
        return "error";
    }

    public function EditPass($userId,$user)
    {
        if(isset($user))
        {
            $this->db->where('userId', $userId);
            $this->db->update('users',$user);
            return "success";
        }
        return "error";
    }

    public function GetUserData($userId)
    {
        $data = new StdClass();
        
        $sql = "SELECT u.userId, userName, name, lastName, teamLeaderId, cuil, roleId 
        FROM users u
        WHERE u.userId = ? and u.active = 1";
        $data->user = $this->db->query($sql,$userId)->row();

        $hierarchyId = $this->Identity_model->getHierarchy($this->session->UserId);
        $sql = "SELECT roleId,name FROM roles WHERE hierarchyId >= ?";
        $data->roles = $this->db->query($sql,$hierarchyId)->result();
        
        $sql = "SELECT teamLeaderId,name FROM teamLeaders";
        $data->teamLeaders = $this->db->query($sql)->result();

        $sql = "SELECT c.campaignId,c.name FROM usersInCampaigns uc INNER JOIN campaigns c ON c.campaignId = uc.campaignId WHERE uc.userId = ? AND c.active = 1";
        $data->user_campaigns = $this->db->query($sql,$userId)->result();

        $data->turns = getTurns();
        
        return $data;
    }

    public function Delete($user)
    {   
        if(isset($user))
        {
            $this->db->set('active', 0);
            $this->db->where('userId', $user['userId']);
            $this->db->update('users');

            $this->db->where('userId', $user['userId']);
            $this->db->delete('usersInCampaigns');

            return "success";
        }
        return "error";
    }

    public function GetDetailDeleteData($userId)
    {
        $sql = "SELECT u.userId,userName,password, u.name, lastName, turn, cuil, createDate, t.name teamLeader, cuil, r.name role,h.name hierarchy
        FROM users u
        LEFT JOIN roles r
        ON (u.roleId = r.roleId)
        LEFT JOIN hierarchy h
        ON r.hierarchyId = h.hierarchyId
        LEFT JOIN teamLeaders t
        ON (u.teamLeaderId = t.teamLeaderId)
        WHERE u.userId = ? AND u.active = 1";
        $res = $this->db->query($sql,$userId)->row();

        $sql = "SELECT c.name,
        c.campaignId
        FROM usersInCampaigns uc
        INNER JOIN campaigns c ON c.campaignId = uc.campaignId
        WHERE uc.userId = ?";
        $res->user_campaigns = $this->db->query($sql,$userId)->result();

        return $res;
    }

    public function SearchUsers($searchValue)
    {
        $sql = 'SELECT userId, CONCAT(name, " ", lastName) as "completeName"
        FROM users
        WHERE LOWER(CONCAT(name, " ", lastName)) LIKE ? AND active = 1
        ORDER BY completeName
        LIMIT 5';

        return $this->db->query($sql, '%'.$searchValue.'%')->result();
    }

    public function InsertUserCampaigns($userCampaigns)
    {
        if (isset($userCampaigns)) {
            $this->db->insert_batch('usersInCampaigns', $userCampaigns);
            return true;
        }

        return false;
    }
}

/* End of file Users_model.php */


?>