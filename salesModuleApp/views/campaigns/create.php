	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo($this->lang->line('campaign_create')) ?>
			</h3>
		</div>
		<div class="card-body">
			<?php
			if(isset($error))
			{
				echo('<div class="alert alert-danger">'.$error.'</div>');
			}?>
			<form method="POST" action="">
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="font-weight-bold" for="campaign_name"><?php echo($this->lang->line('campaign_name')) ?><i class="text-danger">&nbsp;*</i></label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="campaign_name" id="campaign_name" required minlength="1" maxlength="50" value="<?php echo(set_value('campaign_name'))?>"></input>
						<?php echo(form_error('campaign_name'))?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="font-weight-bold" for="campaign_description"><?php echo($this->lang->line('campaign_description')) ?></label>
					</div>
					<div class="col-sm-10">
						<textarea class="form-control" name="campaign_description" id="campaign_description" minlength="0" maxlength="100"><?php echo(set_value('campaign_description')) ?></textarea>
						<?php echo(form_error('campaign_description'))?>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-sm-2">
					</div>
					<div class="col-sm-2">
						<input style="margin-left:0.1em" type="checkbox" class="check" id="campaign_extra_data"/>
						<input type="hidden" name="campaign_extra_data_h" value="0"/>
						<label class="font-weight-bold" for="campaign_extra_data"><?php echo($this->lang->line('campaing_extra_data')) ?></label>
					</div>
				</div>

				<div class="form-group row d-none" id="exd">
					<div class="col-sm-2">
					</div>
					<div class="col-sm-10">
						<input type="checkbox" class="check" id="campaign_extra_data_required" onclick="$(this).next().val(this.checked?1:0)"/>
						<input type="hidden" name="campaign_extra_data_required_h" value="0"/>
						<label class="font-weight-bold" for="campaign_extra_data_required"><?php echo($this->lang->line('campaign_extra_data_required')) ?></label>
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="font-weight-bold"><?php echo($this->lang->line('campaign_products')) ?></label>
					</div>
					<div class="col-sm-10 text-center">
						<a class="float-right btn btn-success" id="add_product">
							<i class="fa fa-plus text-white"></i><span></span>
						</a>
						<div class="col-sm-10">
							<div class="form-group row">
								<div class="col-sm-2">
									<label class="font-weight-bold form-label"><?php echo($this->lang->line('general_name')) ?><i class="text-danger">*</i></label>
								</div>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="productName" maxlength="20">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-2">
									<label class="font-weight-bold form-label"><?php echo($this->lang->line('product_detail')) ?><i class="text-danger">*</i></label>
								</div>
								<div class="col-sm-10">
									<textarea class="form-control" type="text" id="productDetail" maxlength="100"></textarea>
								</div>
								<p class="text-danger text-center d-none" id="productNoData"><?php echo $this->lang->line('campaign_error_product_required')?></p>
								<p class="text-danger text-center d-none" id="invalidData"><?php echo $this->lang->line('product_input_only_characters')?></p>
								<p class="text-danger text-center d-none" id="productSameName"><?php echo $this->lang->line('campaign_error_product_name_unique_in_list')?></p>
							</div>	
						</div>
						<div class="card col-sm-10" id="campaign_products_show">
							<h6 class="card-header">
								<?php echo $this->lang->line('campaign_added_products')?>
							</h6>
							<?php 
							if (sizeof($this->input->post('campaignProducts')) > 0 ){

								foreach ($this->input->post('campaignProducts') as $key => $value) { ?>
									
									<div class="card-body text-left" name="campaignProducts[]" id="prod_<?php echo $key; ?>">
										<button type="button" class="float-right badge badge-danger" name="deleteButton" id="<?php echo $key; ?>"><i class="fas fa-times"></i></button>
										<div class="form-group row">
											<div class="col-sm-2">
												<label class="font-weight-bold form-label"><?php echo($this->lang->line('general_name')) ?>
												<i class="text-danger">*</i>
											</label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="campaignProducts[<?php echo $key; ?>][name]" type="text"  maxlength="20" value='<?php echo $value['name']; ?>'>
										</div>
										<?php echo(form_error('campaignProducts['.$key.'][name]"'))?>
									</div>
									<div class="form-group row">
										<div class="col-sm-2">
											<label class="font-weight-bold form-label">
												<?php echo($this->lang->line('product_detail')) ?><i class="text-danger">*</i>
											</label>
										</div>
										<div class="col-sm-8">
											<textarea class="form-control" name="campaignProducts[<?php echo $key; ?>][detail]" type="text" maxlength="100"><?php echo $value['detail']; ?></textarea>
										</div>
										<?php echo(form_error('campaignProducts['.$key.'][detail]"'))?>
									</div>	
								</div>
								<?php 
							}
						} 
						?>
					</div>
				</div>
			</div>
			<?php echo validation_errors(); ?>

			<div class="text-center">
				<button type="submit" class="btn btn-success"><?php echo($this->lang->line('general_save')) ?></button>
				<a class="btn btn-danger" href="/<?php echo FOLDERADD; ?>/campaigns"><?php echo($this->lang->line('general_cancel')) ?></a>
			</div>
		</form>
	</div>
</div>
</div>

<script>
	$(document).ready(function()
	{	

		var timePress, productIndex = 0;
		
		$('#add_product').on('click', confirmProduct);
		function confirmProduct() {

			var product = {
				name: $('<span>').text($('#productName').val()).html(),
				detail: $('<span>').text($('#productDetail').val().trim()).html(),
			};

			if(product.name.length == 0 || product.detail.length == 0){

				$('#productNoData').toggleClass('d-none',false);
			}
			else{
				$('#campaign_products_show').append('<div class="card-body text-left" name="campaignProducts[]" id="prod_'+productIndex+'"><button type="button" name="deleteButton" id="'+productIndex+'" class="float-right badge badge-danger"><i class="fas fa-times"></i></button><div class="form-group row"><div class="col-sm-2"><label class="font-weight-bold form-label"><?php echo($this->lang->line('general_name')) ?><i class="text-danger">*</i></label></div><div class="col-sm-8"><input class="form-control" name="campaignProducts['+productIndex+'][name]" type="text" maxlength="20" value="'+product.name+'"></div></div><div class="form-group row"><div class="col-sm-2"><label class="font-weight-bold form-label"><?php echo($this->lang->line('product_detail')) ?><i class="text-danger">*</i></label></div><div class="col-sm-8"><textarea class="form-control" name="campaignProducts['+productIndex+'][detail]" type="text" maxlength="100">'+product.detail+'</textarea></div></div></div>');


				$('#productName').val('');
				$('#productDetail').val('');
				productIndex++;


			}
		}

		$('#campaign_products_show').on('click','[name="deleteButton"]',removeProduct);
		function removeProduct() {

			var selectedProduct = $(this);

			$('#campaign_products_show').children('[id="prod_'+selectedProduct.attr('id')+'"]').remove();

			count = 0;

			Array.prototype.forEach.call($('#campaign_products_show').children('[name="campaignProducts[]"]'), child => {
				child.id = 'prod_'+count;
				count++;
			});

			count = 0;

			Array.prototype.forEach.call($('[name="deleteButton"]'), child => {
				child.id = count;
				count++;
			});

			productIndex = count;
		}

		$('#campaign_extra_data').on('click',function () {
			$(this).next().val(this.checked?1:0);
			if($(this).prop("checked") == true){
				$('#exd').removeClass("d-none")
            }
            else{
                $('#exd').addClass("d-none")
            }
		})

	});
</script>



