	<?php $this->load->view('_shared/_admin_nav.php') ?>
	<?php
	if($error)
	{
		echo('<div class="alert alert-danger">'.$error.'</div>');
	}?>
	<div class="alert alert-danger">
		<strong><i class="fas fa-skull"></i></strong>
		<?php echo($this->lang->line('campaign_delete_alert')); ?>
	</div>
	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo($this->lang->line('campaign_delete')); ?>
			</h3>
		</div>
		<div class="card-body">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<dl>
							<dt><?php echo($this->lang->line('campaign_name').':'); ?></dt>
							<dd><?php echo($campaign_data['campaign']->name); ?></dd>
						</dl>
					</div>
					<div class="col-md-6">
						<dl>
							<dt><?php echo($this->lang->line('campaign_description').':'); ?></dt>
							<dd><?php echo($campaign_data['campaign']->description); ?></dd>
						</dl>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<dl>
							<dt><?php echo($this->lang->line('main_users').':'); ?></dt>
							<dd>
								<div>
									<?php
									$usersCount = count($campaign_data['users']);

									for($i = 0; $i < $usersCount; $i++)
									{
										echo('<a href="/'<?php echo FOLDERADD; ?>'/users/view/'.$campaign_data['users'][$i]->userId.'" class="badge badge-info" style="margin-right:5px">'.$campaign_data['users'][$i]->completeName.' </a>');
									}

									?>
								</div>
							</dd>
						</dl>
					</div>
					<div class="col-md-6">
						<dl>
							<dt><?php echo($this->lang->line('campaign_products').':'); ?></dt>
							<dd>
								
								<?php
								$productsCount = count($campaign_data['products']);
								if($productsCount == 0)
								{
									echo('<p>'.$this->lang->line('products_empty').'</p>');
								}
								else
								{
									foreach ($campaign_data['products'] as $key => $product) { ?>
										<a class="badge badge-info" id="<?php echo $key; ?>" data-toggle="collapse" href="" name="productView">
											<?php echo $product->name; ?>
										</a>
										<?php 
									}
								}?>
								<div class="collapse" id="productDetail">
									
								</div>
							</dd>
						</dl>
					</div>
				</div>
				<form method="POST" action="">
					<input type="hidden" name="campaign_id" value="<?php echo(set_value('campaign_id', $campaign_data['campaign']->campaignId)) ?>" />
					<div class="text-center">
						<button type="submit" class="btn btn-success"><?php echo($this->lang->line('general_delete')) ?></button>
						<a class="btn btn-danger" href="/<?php echo FOLDERADD; ?>/campaigns"><?php echo($this->lang->line('general_cancel')) ?></a>
						<?php echo(form_error('campaign_id'))?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#delete').addClass('active');
		var products = <?php echo json_encode($campaign_data['products']); ?>;
		
		$('[name="productView"]').on('click',showProductDetails)
		function showProductDetails() {

			$('#productDetail').hide();
			$('#productDetail').empty();
			
			var description = products[parseInt($(this).attr('id'))].detail;
			$('#productDetail').append('<span>'+description+'</span>');
			$('#productDetail').show('slow');
		}
	})
</script>