	<div class="container">
		<div class="card">
			<div class="card-header">
				<h1><?php echo($this->lang->line('campaign_campaings')) ?></h1>
			</div>
			<?php if ($this->Identity_model->Validate('campaigns/admin')){ ?>
				<div class="card-body">
					<?php
					if($this->session->flashdata('deleteResult')){ ?>

						<div class="alert alert-info alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong><i class="fas fa-check"></i></strong>
							<?php echo($this->session->flashdata('deleteResult')); ?>
						</div>
					<?php }?>
					<a class="btn btn-sm btn-outline-success" href="/<?php echo FOLDERADD; ?>/campaigns/create">
						<i class="fas fa-plus"></i>
						<strong><?php echo($this->lang->line('campaign_create')) ?></strong>
					</a>
				</div>
			<?php } ?>
			<div class="card-body">
				<table id="data_table" class="table table-hover">
					<thead>
						<tr>
							<th><?php echo($this->lang->line('campaign_name')) ?></th>
							<th><?php echo($this->lang->line('campaign_users_count')) ?></th>
							<th><?php echo($this->lang->line('campaign_products_count')) ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($campaigns as $item) {?>
							<tr class="optionsUser">
								<td> <?php echo $item->name ?></td>
								<td> <?php echo $item->userCount ?></td>
								<td> <?php echo $item->productsCount ?></td>
								<td class="text-right">
									<?php if ($this->Identity_model->Validate('campaigns/admin')){ ?>
										<a href="/<?php echo FOLDERADD; ?>/campaigns/view/<?php echo $item->campaignId ?>" title="<?php echo($this->lang->line('general_details')) ?>"><i class="fas fa-search"></i></a> &nbsp;
										<a href="/<?php echo FOLDERADD; ?>/campaigns/edit/<?php echo $item->campaignId ?>" title="<?php echo($this->lang->line('general_edit')) ?>"><i class="fas fa-edit text-warning"></i></a> &nbsp;
										<a href="/<?php echo FOLDERADD; ?>/campaigns/delete/<?php echo $item->campaignId ?>" title="<?php echo($this->lang->line('general_delete')) ?>"><i class="fas fa-trash-alt text-danger"></i></a> &nbsp;

									<?php } ?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function(){

			var itemName = "Campañas";

			$('#data_table').DataTable({
				"language": {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ "+itemName,
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando "+itemName+" del _START_ al _END_ de un total de _TOTAL_ "+itemName,
					"sInfoEmpty":      "Mostrando "+itemName+" del 0 al 0 de un total de 0 "+itemName,
					"sInfoFiltered":   "(filtrado de un total de _MAX_ "+itemName+")",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			});
		});
	</script>