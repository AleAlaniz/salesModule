    <?php $this->load->view('_shared/_admin_nav.php') ?>
    <?php if (!$topHierarchy && $higherLevel){ ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_hierarchy_areyousure_delete')); ?>
        </div>
    <?php } ?>

    <?php if(isset($_SESSION['rolesInUse'])){

        $listOfRoles = '';
        foreach ($_SESSION['rolesInUse'] as $role) {
            $listOfRoles = $listOfRoles.$role->name.'<br>';
        }
        echo '
        <div class="modal" tabindex="-1" role="dialog" id="deleteError">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title">'.$this->lang->line('admin_hierarchy_delete_error').'</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        
                    </div>
                    <div class="modal-body">
                        <p class="text-center">'.$this->lang->line('admin_hierarchy_delete_error_text').'</p>
                        <p class="text-center">'.$listOfRoles.'</p>
                    </div>
                </div>
            </div>
        </div>';
    } 
    ?>
    
    <div class="card">
        <div class="card-header">
            <h3><?php echo $this->lang->line('admin_hierarchy_details') ?></h3>
        </div>
        <div class="card-body">
            <div class="row col-md-12">
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('general_name');?> : </dt>
                    <dd><?php echo $currentHierarchy->name ?></dd>
                </dl>
            </div>
            <div class="row">
                <div class="mx-auto mt-3">
                    <form method="POST" novalidate action="/<?php echo FOLDERADD; ?>/hierarchy/deleteHierarchy/<?php echo $currentHierarchy->hierarchyId; ?>">
                        <input type="hidden" name="hierarchyId" value="<?php echo $currentHierarchy->hierarchyId?>">
                        <?php if (!$topHierarchy && $higherLevel){ ?>
                            <button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_delete');?></button>
                            <a href="/<?php echo FOLDERADD; ?>/hierarchy" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                        <?php } else { ?>
                            <div class="alert alert-danger">
                                <strong><i class="fas fa-skull"></i></strong>
                                <?php echo($this->lang->line('admin_hierarchy_no_permission')); ?>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $("#delete").addClass("active");

            if($('#deleteError')){
                $('#deleteError').modal('show');
            }
        })
    </script>