<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_hierarchy') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('hierarchy/admin')) { ?>
            <div class="card-body">
                <a href="/<?php echo FOLDERADD; ?>/hierarchy/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_hierarchy_create');?></strong></a>
            </div>
        <?php } ?>
        <div class="card-body">
            
            <?php if(isset($_SESSION['hierarchyMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['hierarchyMessage'] == 'create'){
                        echo $this->lang->line('admin_hierarchy_successmessage');
                    }
                    elseif ($_SESSION['hierarchyMessage'] == 'edit'){
                        echo $this->lang->line('admin_hierarchy_editmessage');
                    }
                    elseif ($_SESSION['hierarchyMessage'] == 'delete'){
                        echo $this->lang->line('admin_hierarchy_deletemessage');
                    }

                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="hierarchy">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($hierarchy as $hierarchy ) { ?>
                            <tr class="optionsUser">
                                <td><?php echo $hierarchy->name ?></td>
                                <td class="text-right">
                                    <?php if($this->Identity_model->Validate('hierarchy/admin')){ ?>
                                        <a href="/<?php echo FOLDERADD; ?>/hierarchy/edit/<?php echo $hierarchy->hierarchyId?>" title="<?php echo $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a>&nbsp; 
                                        <a href="/<?php echo FOLDERADD; ?>/hierarchy/delete/<?php echo $hierarchy->hierarchyId?>" title="<?php echo $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a>
                                    <?php }?>
                                </td>
                            </tr>
                <?php   } ?>
                </tbody>
            </table>
        </div>
</div>
<script>
    $(function () {
        $('#hierarchy').DataTable({
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ Jerarquías",
					"sZeroRecords":    "<i class='fa fa-hierarchy'></i> No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando Jerarquías del _START_ al _END_ de un total de _TOTAL_ Jerarquías",
					"sInfoEmpty":      "Mostrando Jerarquías del 0 al 0 de un total de 0 Jerarquías",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ Jerarquías)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
        });
    })
</script>