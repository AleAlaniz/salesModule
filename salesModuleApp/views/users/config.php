<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_users_config');?></h3>
    </div>
    <div class="card-body">
        <ul class="list-group">
            <li class="list-group-item">
                <a class="btn btn-outline-success" href="/<?php echo FOLDERADD; ?>/users/editData" class="pull-right">Editar Datos</a>
            </li>
            <li class="list-group-item">
                <a class="btn btn-outline-success" href="/<?php echo FOLDERADD; ?>/users/changePass" class="pull-right">Cambiar Contraseña</a>
            </li>
        </ul>
    </div>
</div>