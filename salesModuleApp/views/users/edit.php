<?php $this->load->view('_shared/_admin_nav.php') ?>
<div class="card">
    <div class="card-header ">
        <h3><?php echo $this->lang->line('admin_users_edit');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">

            <div class="form-group row">
                <label class="col-md-2" for="name"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="name" name="name" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" value="<?php echo $user->name ?>">
                    <p class="text-danger"><?php echo form_error('name'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="lastName"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_lastName');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="lastName" name="lastName" type="text" placeholder="<?php echo $this->lang->line('admin_users_lastName');?>" value="<?php echo $user->lastName ?>">
                    <p class="text-danger"><?php echo form_error('lastName'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 pr-4" for="userName"><span class="font-weight-bold"><?php echo $this->lang->line('login_username');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="userName" name="userName" type="text" placeholder="<?php echo $this->lang->line('login_username');?>" value="<?php echo $user->userName ?>">
                    <p class="text-danger"><?php echo form_error('userName'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="turn"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_turn');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="turn" id="turn" class="form-control" >
                        <?php 
                        foreach ($turns as $turn) {
                            echo "<option value='$turn'>$turn</option>";
                        }
                        ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('turn'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="turn">
                    <span class="font-weight-bold">
                        <?php echo $this->lang->line('main_campaigns');?>:
                    </span>
                </label>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('general_search')) ?>" id="input_search" autocomplete="off">
                </input>
                <?php echo(form_error('user_campaigns[]'))?>
                <span class="fas fa-sync fa-spin cat-inside-input text-primary" style="display: none" id="loadingSpinner"></span>
                <ul class="list-group" id="search_result">      
                </ul>  
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-2">
                <label class="font-weight-bold"><?php echo($this->lang->line('admin_users_campaign')) ?></label>
            </div>
            <div class="col-sm-10" id="campaigns_added_show">
                <?php if(isset($user_campaigns)) {

                    $user_campaigns_count = sizeof($user_campaigns);

                    for ($i=0; $i < $user_campaigns_count; $i++) { 

                        echo('<span class="badge badge-info" style="margin-right:5px">'.$user_campaigns[$i]->name.'<span name="userSelected" class="badge badge-pill badge-danger btn" id="btn_'.$user_campaigns[$i]->campaignId.'"><i class="fas fa-times"></i></span></span>');
                    }
                }?>
                <select name="user_campaigns[]" multiple id="campaigns_added_send" style="display: none;">
                    <?php if(isset($user_campaigns)) {

                        $user_campaigns_count = sizeof($user_campaigns);

                        for ($i=0; $i < $user_campaigns_count; $i++) { ?>

                            <option value="<?php echo $user_campaigns[$i]->campaignId; ?>" selected></option>   
                            <?php
                        }
                    }?>    
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-2" for="teamLeader"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_teamleader');?>:</span></label>
            <div class="col-md-10">
                <select name="teamLeader" id="teamLeader" class="form-control" >
                    <option value="EMPTY"></option>
                    <?php 
                    foreach ($teamLeaders as $teamLeader) { ?>
                        <option value="<?php echo $teamLeader->teamLeaderId?>" <?php echo  set_select('teamLeader', $teamLeader->teamLeaderId, ($user->teamLeaderId == $teamLeader->teamLeaderId)); ?> ><?php echo $teamLeader->name?></option>
                    <?php } ?>
                </select>
                <p class="text-danger"><?php echo form_error('teamLeader'); ?></p>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-2" for="cuil"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_cuil');?>:</span><span class="text-danger"><strong> *</strong></span></label>
            <div class="col-md-10">
                <input class="form-control" id="cuil" name="cuil" type="text" placeholder="<?php echo $this->lang->line('admin_users_cuil');?>" value="<?php echo $user->cuil ?>" maxlength="11">
                <p class="text-danger"><?php echo form_error('cuil'); ?></p>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-2" for="role"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_role');?>:</span></label>
            <div class="col-md-10">
                <select name="role" id="role" class="form-control" >
                    <option value="EMPTY"></option>
                    <?php 
                    foreach ($roles as $role) { ?>
                        <option value="<?php echo $role->roleId?>" <?php echo  set_select('role', $role->roleId, ($user->roleId == $role->roleId)); ?> ><?php echo $role->name?></option>
                    <?php } ?>
                    ?>
                </select>
                <p class="text-danger"><?php echo form_error('role'); ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_users_edit');?></button>
                <a href="/<?php echo FOLDERADD; ?>/users" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
            </div>
        </div>
    </form>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('#edit').addClass('active');

        var timePress;

        $("#campaigns_added_show").on('click', '[name="userSelected"]', deleteCampaign);
        function deleteCampaign() {

            var selectedUser = $(this);
            selectedUser.parent().remove();
            $('#campaigns_added_send').children('[value="'+selectedUser.attr('id').split('_')[1]+'"]').remove();
        }

        $("#search_result").on('click', '[name="searchOption"]', addCampaign);
        function addCampaign(){

            var campaignId = $(this).attr('id');

            $('#campaigns_added_show').prepend('<span class="badge badge-info" style="margin-right:5px">'+$('#'+campaignId).text()+'<span name="userSelected" class="badge badge-pill badge-danger btn" id="btn_'+$('#'+campaignId).attr('id')+'"><i class="fas fa-times"></i></span></span>');
            $('#campaigns_added_send').prepend('<option value="'+$('#'+campaignId).attr('id')+'" selected></option>')
            $('#'+campaignId).remove();
        }

        $('#input_search').on('keyup paste', function()
        {
            $('#loadingSpinner').css('display', 'block');

            if($('#input_search').val().length > 0)
            {
                clearTimeout(timePress);
                timePress = setTimeout(searchUser, 300);
            }
            else
                searchUser();
        });

        function searchUser() {

            $("#search_result").empty();
            var searchText = $('#input_search').val().toLowerCase().trim();

            if (searchText.length > 0){

                $.ajax({   
                    type:     'POST',
                    url:      '/<?php echo FOLDERADD; ?>/campaigns/search_campaigns',
                    data:     {'search':searchText},
                    dataType: 'JSON',                
                    success:  ajaxResponse
                })
            }
            else{

                $('#loadingSpinner').css('display', 'none');
            }

            function ajaxResponse(data)
            {
                var campaingsAdded = $("#campaigns_added_send > option");

                for (var i = 0; i < data.length; i++)
                {
                    var campaignFound = false;

                    for (var j = 0; j < campaingsAdded.length; j++) 
                    {   
                        if (campaingsAdded[j].value == data[i].campaignId)
                        {
                            campaignFound = true;
                        }
                    }

                    if(!campaignFound)
                    {
                        $('#search_result').append('<li class="list-group-item cat-pointer" name="searchOption" id="'+data[i].campaignId+'">'+data[i].name+'</li>');
                    }
                }

                $('#loadingSpinner').css('display', 'none');
            }
        }
    }); 
</script>