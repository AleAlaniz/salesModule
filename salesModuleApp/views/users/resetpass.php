<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_users_resetPass');?></h3>
    </div>
    <div class="card-body">
        <?php if(isset($_SESSION['userMessage']))
        { ?>
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-check"></i></strong> 
                <?php if ($_SESSION['userMessage'] == 'edit'){
                    echo $this->lang->line('admin_users_editmessage');
                } ?>
            </div>
        <?php } ?>
        <form method="POST">
            <div class="form-group row">
                <label class="col-md-2" for="user"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_user_name');?></span>:<span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="user" name="user" type="text" placeholder="<?php echo $this->lang->line('admin_users_user_name');?>" value="<?php echo set_value('user',$this->input->post('user'));?>">
                    <p class="text-danger"><?php echo form_error('user'); ?></p>
                    <?php if(isset($userNotExists) && $userNotExists) { ?>
                        <p class="text-danger"><?php echo  $this->lang->line('admin_users_userNotExists'); ?></p>
                    <?php } ?>
                    <?php if(isset($userNotRightToEdit) && $userNotRightToEdit) { ?>
                        <p class="text-danger"><?php echo  $this->lang->line('admin_users_userNotRightToEdit'); ?></p>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 pr-1" for="password"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_new_password');?></span>:<span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="password" name="password" type="password" placeholder="<?php echo $this->lang->line('admin_users_new_password');?>" >
                    <p class="text-danger"><?php echo form_error('password'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 pr-1" for="confirmPassword"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_confirm_password');?></span>:<span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="confirmPassword" name="confirmPassword" type="password" placeholder="<?php echo $this->lang->line('admin_users_confirm_password');?>" >
                    <p class="text-danger"><?php echo form_error('confirmPassword'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_users_edit');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/users" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
    </form>
</div>