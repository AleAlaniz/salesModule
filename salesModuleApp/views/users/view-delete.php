    <?php $this->load->view('_shared/_admin_nav.php') ?>
    <?php if($type == "delete") { ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_areyousure_delete_user')); ?>
        </div>
    <?php } ?>    
    
    <div class="card">
        <div class="card-header">
            <h2><?php echo $this->lang->line('admin_users_details') ?></h2>
        </div>
        
        <div class="card-body">
            <div class="row col-md-12">
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('general_name');?> : </dt>
                    <dd><?php echo $user->name ?></dd>
                    <dt><?php echo $this->lang->line('admin_users_lastName');?> : </dt>
                    <dd><?php echo $user->lastName ?></dd>
                    <dt><?php echo $this->lang->line('login_username');?> : </dt>
                    <dd><?php echo $user->userName ?></dd>
                    <dt><?php echo $this->lang->line('admin_users_turn');?> : </dt>
                    <dd><?php echo $user->turn ?></dd>
                    <dt><?php echo $this->lang->line('main_campaigns');?> : </dt>
                    <dd>
                        <?php 

                        if(isset($user->user_campaigns)) {

                            $user_campaigns_count = sizeof($user->user_campaigns);

                            if ($user_campaigns_count > 0) {

                                foreach ($user->user_campaigns as $campaign) { ?>

                                    <a class="badge badge-info" style="margin-right:5px; color:white;" href="/<?php echo FOLDERADD; ?>/campaigns/view/<?php echo $campaign->campaignId; ?>" target="_blank">
                                        <?php echo $campaign->name; ?>
                                    </a>   
                                    <?php
                                }

                            }
                            else{
                                echo ('<span>'.$this->lang->line('general_undefined').'</span>');
                            }
                        }?>

                    </dd>
                </dl>
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('admin_users_teamleader');?> : </dt>
                    <dd><?php echo($user->teamLeader ? $user->teamLeader : $this->lang->line('general_undefined')) ?></dd>
                    <dt><?php echo $this->lang->line('admin_users_cuil');?> : </dt>
                    <dd><?php echo $user->cuil ?></dd>
                    <dt><?php echo $this->lang->line('admin_users_role');?> : </dt>
                    <dd><?php echo($user->role ? $user->role : $this->lang->line('general_undefined')) ?></dd>
                    <dt><?php echo $this->lang->line('admin_hierarchy_level');?> : </dt>
                    <dd><?php echo($user->hierarchy) ?></dd>
                </dl>
            </div>
            <?php if($type == "delete") { ?>
                <div class="row">
                    <div class="mx-auto mt-3">
                        <form method="POST">
                            <input type="hidden" name="userId" value="<?php echo $user->userId?>">
                            <button class="btn btn-success"><?php echo  $this->lang->line('general_delete') ?></button>
                            <a href="/<?php echo FOLDERADD; ?>/users" class="btn btn-danger"><?php echo  $this->lang->line('general_cancel') ?></a>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php if($type == "delete") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#delete").addClass("active");
        	})
        </script>
    <?php } ?>
    <?php if($type == "view") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#view").addClass("active");
        	})
        </script>
        <?php } ?>